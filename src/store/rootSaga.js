import { takeLatest } from "redux-saga/effects"
import ACTION from "./types"
import { 
    addReview
} from "./sagas/userSagas"

export default function* rootSaga() {
    //add review
    yield takeLatest (ACTION.ADD_REVIEW, addReview);
    

} 