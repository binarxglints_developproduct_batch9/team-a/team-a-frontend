/* eslint-disable import/no-anonymous-default-export */
import axios from "axios"

export default {
    review: {
        //ADD REVIEW
        reviewAdd: addReview => 
            axios.post(`https://sayonara.kuyrek.com/review/create/${addReview.productId}`,{comment: addReview.comment, rating: addReview.rating},
            {
                'headers': {'Authorization': `Bearer ${localStorage.getItem('token')}`} 
            }).then (res => res.data.data).then(()=> {
                setTimeout(console.log('bisa plis'), 500)
                window.location.href=`/productDetail/${addReview.productId}`
            }),

        //GET REVIEW

        reviewGet: getReview => 
        axios.get(`https://sayonara.kuyrek.com/review/${getReview.productId}`,
        {
            'headers': {'Authorization': `Bearer ${localStorage.getItem('token')}`} 
        }).then (res => res.data.data)

    }

}