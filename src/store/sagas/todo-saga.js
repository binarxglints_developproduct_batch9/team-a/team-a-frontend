// Import the redux-saga/effects
import {
    put,
    call,
    takeLatest,
    takeEvery
  } from 'redux-saga/effects'
  
  // Import all actions and api's
  import {
    SET_LOADING,
    GET_SIGNIN,
    GET_SIGNUP,
    PUT_SIGNIN,
    PUT_SIGNUP
  } from '../actions/todo-action'
  
  // Import all api's
  import {
    getSignInApi
  } from '../api/todo-api'
  
  // Here's the unique part, generator function*, function with asterisk(*)
  
  // Get Todos
  function* getSignIn(data) {
    yield put({ type: SET_LOADING })

    const todos = yield call(getSignInApi, data)
    console.log(todos)
    alert("adaa")
    yield put({ type: PUT_SIGNIN, payload: todos })
  }

//   function* getSignUp() {
//     yield put({ type: SET_LOADING })
  
//     const todos = yield call(getAllTodos)
  
//     yield put({ type: GET_SIGNUP, payload: todos })
//   }
  
//   // Set the title of todo
//   function* setTodoTitle({ payload }) {
//     yield put({ type: SET_TODO_TITLE, payload })
//   }
  
//   // Create Todo
//   function* createTodo({ payload }) {
//     yield put({ type: SET_LOADING })
  
//     const newTodo = yield call(createNewTodo, payload)
  
//     yield put({ type: CREATE_TODO, payload: newTodo })
    
//     // Clear todo after creating
//     yield put({ type: CLEAR_TODO_TITLE })
//   }
  
//   // Delete todo
//   function* deleteTodo({ payload }) {
//     yield put({ type: SET_LOADING })
  
//     const todo = yield call(deleteExistedTodo, payload)
  
//     yield put({ type: DELETE_TODO, payload: todo })
//   }
  
  // Export the saga (todo-saga)
  export default function* todoSaga() {
    yield takeLatest(GET_SIGNIN, getSignIn)
    // yield takeEvery(GET_SIGNUP, getSignUp)
    // yield takeLatest(CREATE_TODO_REQUESTED, createTodo)
    // yield takeEvery(DELETE_TODO_REQUESTED, deleteTodo)
  }