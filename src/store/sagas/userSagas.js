import { call, put } from "redux-saga/effects"
import { 
    reviewAdded,
    // reviewAddFailed
 } from "../actions/users"
 import api from "../api"

 export function* addReview({payload}) {
    try{
        const user = yield call (api.review.reviewAdd, payload);
        yield put (reviewAdded(user));
    }catch(err) {
        // yield put (reviewAddFailed(err));
        console.log(err);
    }
     
 }