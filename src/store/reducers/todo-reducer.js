import { 
    SET_LOADING,
    GET_SIGNIN,
    GET_SIGNUP,
    PUT_SIGNIN,
    PUT_SIGNUP
  } from '../actions/todo-action'
  
  
  // Define your state here
  const initialState = {
    loading: false,
    todos: [],
    title: ''
  }
  
  // This export default will control your state for your application
  export default(state = initialState, {type, payload}) => {
    switch(type) {
      // Set loading
      case SET_LOADING:
        return {
          ...state,
      loading: true
        }
      // Get todos
      case PUT_SIGNIN:
        return {
          ...state,
          todos: payload,
          loading: false
        }
      // Set todo title from user that gonna input a title in form
      case PUT_SIGNUP:
        return {
          ...state,
      title: payload
        }
        default :
        return state
    }
  }