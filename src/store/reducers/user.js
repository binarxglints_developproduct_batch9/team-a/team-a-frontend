import ACTION from "../types"

export default function user (state = {}, action = {}) {
    switch (action.type) {
        case ACTION.ADD_REVIEW_SUCCESS:
            return {...action.user}
        case ACTION.ADD_REVIEW:
            return {...action.user}
        case ACTION.GET_REVIEW_SUCCESS:
            return {...action.user}
        case ACTION.GET_REVIEW:
            return {...action.user}
        default:
            return state
    }
}