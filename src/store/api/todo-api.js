import defaultAxios from 'axios'

const axios = defaultAxios.create({
  baseURL: 'http://122.248.229.2:3003/',
  headers: {'Content-Type': 'application/json'}
});

// Get All Todos
export const getSignInApi = async (data) => {
    console.log(data)
  try {
    const todos = await axios.post('login', data.payload)
    console.log(todos, data)
    alert(todos)
    return todos.data
  } catch(err) {
    alert("kosong")
    return console.error(err)
  }
}

// Create New Todo
export const createNewTodo = async (title) => {
  try {
    const todo = await axios.post('todos', {
      title
    })

    return todo.data
  } catch(err) {
    return console.error(err)
  }
}

// Delete existed todo
export const deleteExistedTodo = async (id) => {
  try {
    await axios.delete(`todos/${id}`)
    
    return id
  } catch(err) {
     return console.error(err)
  }
}