import axios from "axios";
import actionTypes from "../constant/actionType"

export const addReview = (productId) => {
    return async (dispatch) => {
        dispatch ({
            type: actionTypes.LOADING_REVIEW,
            payload: {
                loading: true,
            }
        })
        console.log(productId, "test productid action");
        try {
            const comment = await axios.post (`https://sayonara.kuyrek.com/review/create/${productId}`)
            dispatch({
                type: actionTypes.ADD_REVIEW,
                payload: {
                    review: comment.data.data,
                    loading: false
                }
            })
        } catch (err) {
            console.log(err, "error action review");

        }
    }

}