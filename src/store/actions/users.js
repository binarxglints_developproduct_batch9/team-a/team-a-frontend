import ACTION from "../types"

export const reviewAdded = payload => ({
    type: ACTION.ADD_REVIEW_SUCCESS, payload: payload
});
export const addReview = payload => ({
    type: ACTION.ADD_REVIEW, payload: payload
})
