import React, { useEffect, useState } from "react";
import ReactStars from "react-rating-stars-component";
import { Tab, Tabs } from "react-bootstrap";
import img from "../assets/images/profilepicture.png";
import Discussion from "./Dicussion";
import "../assets/css/reviewproduct.scss";
import axios from "axios";
import { useParams, Link } from "react-router-dom";

function Review() {
  let { productId } = useParams();
  console.log(productId, "console productId di product review");
  let idReview = localStorage.getItem("id review");
  console.log(idReview, "coba console id review wkkw");

  const [show, setShow] = useState([{}]);

  useEffect(() => {
    const urlUpdate = `https://sayonara.kuyrek.com/review/${productId}`;
    axios
      .get(urlUpdate)
      .then((res) => {
        const showReview = res.data.data;
        setShow([{ showReview }]);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const result = show[0];
  const { showReview } = result;
  
  

  return (
    <body>
      <div className="container-review">
        <Tabs
          fill
          defaultActiveKey="review"
          id="uncontrolled-tab-example"
          className="tabbtn"
          variant="pills"
        >
          <Tab eventKey="review" title="Review">
            {
              showReview
                ? showReview.map((item) => {
                    const { buyer, rating, comment } = item;
                    return (
                      <div className="wrapper">
                        <div className="card-review">
                          <img
                            src={`https://sayonara.kuyrek.com${buyer.image}`}
                            alt="avatar"
                            className="img-review-profile"
                          />
                          <div className="content">
                            <h5>{buyer.username}</h5>
                            <div className="rate-review">
                              <ReactStars
                                value={rating.$numberDecimal}
                                edit={false}
                              />
                            </div>
                            <div className="box-review">
                              <p className="inside-review">{comment}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    );
                  })
                : null
              // !showReview.length && <p>There's no review yet</p>
            }
            {showReview === undefined
              ? null
              : !showReview.length && (
                  <div className="header-container bg-white p-4 mb-2 h4">
                    <h5 className="color-primary">
                      This product has no review yet.
                    </h5>
                  </div>
                )}
          </Tab>
          <Tab eventKey="discussion" title="Discussion" variant="pills">
            <Discussion />
          </Tab>
        </Tabs>
      </div>
    </body>
  );
}

export default Review;
