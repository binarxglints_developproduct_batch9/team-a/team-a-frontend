import React from "react";
import ProductDetails from "./ProductDetails";
import { createStore } from "redux";
import { Provider } from "react-redux";

function Counter() {
  const initialState = { count: 0 };

  //set reducer
  function reducer(state = initialState, action) {
    switch (action.type) {
      case "ADD":
        return { ...state, count: state.count + 1 };
      case "MIN":
        return { ...state, count: state.count - 1 };
      default:
        return state;
    }
  }

  //set createStore
  const store = createStore(reducer);
  console.log("store created", store.getState());
  store.dispatch({ type: "ADD" });

  return (
    <div>
      <Provider store={store}>
        <ProductDetails />
      </Provider>
    </div>
  );
}

export default Counter;
