import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import img from "../assets/images/profilepicture.png";
import "../assets/css/addreply.scss";
import NewReply from "./NewReply";
import { data } from "jquery";
import { Col, Image, Row } from "react-bootstrap";

function AddReply() {
  let { productId } = useParams();
  console.log(productId, "productid discussion");

  const [show, setShow] = useState([{}]);

  useEffect(() => {
    const urlShow = `https://sayonara.kuyrek.com/discuss/${productId}`;

    axios
      .get(urlShow)
      .then((res) => {
        const resultReply = res.data.data;

        setShow([{ resultReply }]);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const result = show[0];
  const { resultReply } = result;
  console.log(resultReply, "result reply");

  // const { resultReply } = show
  // console.log(resultReply, "data reply");

  return (
    <div className="header-container bg-white rounded p-3 rounded-lg">
      {resultReply
        ? resultReply.map((item) => {
            const { commentator, question, created_at, id, answer } = item;
            // const idDisscusion = {id}
            const split = created_at.split("T");
            return (
              <>
                <Row className="d-flex flex-wrap p-4">
                  <Col md="1">
                    <Image
                      src={`http://122.248.229.2:3003${commentator.image}`}
                      className="rounded-circle"
                      style={{ height: "5vw" }}
                    />
                  </Col>
                  <Col>
                    <p>
                      {commentator.username} asked on {split[0]}
                    </p>
                    <p>{question}</p>
                  </Col>
                </Row>
                {answer.map((ans) => {
                  const { username, id, image, comment } = ans;
                  return (
                    <Row
                      key={id}
                      className="header-container border bg-light rounded p-3 rounded-lg d-flex flex-md-row"
                    >
                      <Col md="1">
                        <Image
                          src={`http://122.248.229.2:3003${image}`}
                          className="rounded-circle"
                          style={{ height: "5vw" }}
                        />
                      </Col>
                      <Col>
                        <p>{username}</p>
                        <p>{comment}</p>
                      </Col>
                    </Row>
                  );
                })}
                <NewReply id={id} />
              </>
            );
          })
        : null}
    </div>
  );
}

export default AddReply;
