import React, { useEffect, useState } from "react";
import { FaRegComments } from "react-icons/fa";
import ModalDiscussion from "./ModalDiscussion";
import "../assets/css/dicussion.scss";
import AddReply from "./AddReply";
import { useParams } from "react-router-dom";
import axios from "axios";

const Discussion = (props) => {
  const [modalDiscussion, setModalDiscussion] = useState(false);
  const toggleModalDiscussion = () => setModalDiscussion(!modalDiscussion);
  const toggleSwitch = () => {
    setModalDiscussion(!modalDiscussion);
  };

  return (
    <body>
      <div>
        <div className="wrapper">
          <div className="card-discussion">
            <div className="content-discussion">
              <FaRegComments size={70} />
              <p>
                Do you have any questions? Discuss it with our Admin and other
                users
              </p>
              <button
                className="btn-add-disscussion"
                onClick={toggleModalDiscussion}
              >
                Add Questions
              </button>

              <ModalDiscussion
                modal={modalDiscussion}
                toggleModalDiscussion={toggleModalDiscussion}
                toggleSwitch={toggleSwitch}
              />
            </div>
          </div>
        </div>
      </div>
      <AddReply />
    </body>
  );
};

export default Discussion;
