import axios from "axios";
import React, { useState } from "react";
import { useParams } from "react-router-dom";
import { ModalBody, Modal, ModalHeader } from "reactstrap";
import { useFormik } from "formik";
import "../assets/css/modaldiscussion.scss";

function ModalDiscussion(props) {
  let { productId } = useParams();
  console.log(productId, " test console productId");

  const [questions, setQuestions] = useState({
    question: "",
  });

  let discuss;
  const addDiss = (e) => {
    e.preventDefault();
    console.log(questions, " wow question");

    setQuestions({
      question: discuss,
    });

    const urlAddDiss = `https://sayonara.kuyrek.com/discuss/${productId}/create`;
    axios
      .post(urlAddDiss, questions)
      .then((res) => {
        console.log(res);
        alert("Success adding question!");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <body>
      <div>
        <Modal isOpen={props.modal} toggle={props.toggleModalDisucssion}>
          <ModalBody>
              <form className="content-question" onSubmit={addDiss}>
                <label className="label-discussion" for="discussion">
                  <b>Question:</b>
                </label>
                <input
                  className="input-discussion"
                  onChange={(e) => (discuss = e.target.value)}
                />
                <button className="btn-question" type="submit">
                  Submit Question!
                </button>
              </form>
          </ModalBody>
        </Modal>
      </div>
    </body>
  );
}
export default ModalDiscussion;
