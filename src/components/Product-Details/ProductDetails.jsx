import React, { useState, useEffect } from "react"
import "../assets/css/ProductDetails.scss"
import { FaShoppingCart } from "react-icons/fa"
import axios from "axios"
import { Link, useParams } from "react-router-dom"
import ReactStars from "react-rating-stars-component"
import { Button, Modal } from "react-bootstrap"

function Product() {
  const ratingChanged = (newRating) => {
    console.log(newRating);
  };

  const [product, setProduct] = useState({
    productData: [],
  });

  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
  }

  let { productId } = useParams()

  useEffect(() => {
    const url = `https://sayonara.kuyrek.com/home/${productId}`;

    axios
      .get(url)
      .then((res) => {
        const resultProduct = res.data.data;
        setProduct({
          productData: resultProduct,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const { productData } = product;
  console.log(productData, "product detail");

  const { image, desc, name, price, stock, seller } = productData;

  //create counter for qty
  const [count, setCount] = useState(0);
  const stocks = stock === null || stock === undefined ? null : stock.$numberDecimal;

  function add() {
    if (count < stocks) setCount(count + 1)
  }
  function min() {
    if (count > 0) {
      setCount(count - 1);
    }
  }

  const addToCart = (add, item) => {
    const urlCart = `https://sayonara.kuyrek.com/cart/create`;

    console.log(item, "item");
    axios
      .post(urlCart, {
        productId: productId,
      })
      .then((res) => {
        setError(false)
        setTitle('Success')
        setMessage('Item added to cart.')
        setModal(true)
      })
      .catch((err) => {
        setError(err.message)
        setTitle('Error')
        if(err.message === 'Request failed with status code 401') setMessage('Please Sign In first!')
        else if(err.message === 'Request failed with status code 422') setMessage(`You can't buy your own product!`)
        setModal(true)
      });
  };

  const username = seller === undefined ? null : seller.username;
  const foto = seller === undefined ? null : seller.image;

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };

  return (
    <body>
      <div className="container-product">
        <div className="wrappers">
          <div className="card-product">
            <img
              src={`https://sayonara.kuyrek.com/${image}`}
              alt="banana"
              className="img-product"
            />
            <div className="content">
              <h3 className="h3-product">{name}</h3>
              <div className="desc-box">
                <h5>{desc}</h5>
              <ReactStars
                edit={false}
                count={5}
                value={4.5}
                isHalf={true}
                size={24}
              />
              </div>
              <div className="box-price">
                Rp {price === undefined ? null : priceForm(price.$numberDecimal)},- /kg
              </div>
              <div className="product-quantity">
                <p>Quantity:</p>
                <button onClick={add} className="btn-add-qty">
                  +
                </button>
                <span>{count}</span>
                <button onClick={min} className="btn-min-qty">
                  -
                </button>
                <div className="product-stock">
                  <p>Stock :</p>
                  <p>{stocks}</p>
                </div>
              </div>
              <divs className="profile-seller-product">
                <img
                  src={`https://sayonara.kuyrek.com${foto}`}
                  alt="profile"
                  className="profile-photo-seller"
                />
                <div className="content-profile-product">
                  <p>{seller === undefined ? null : seller.username}</p>

                  <Link to={`/seller/${username}`}>
                    <button className="btn-seller-details">View Profile</button>
                  </Link>
                </div>
              </divs>
              <div className="btn-cart">
                <button
                  className="btn-add-to-cart"
                  onClick={(add) => addToCart(add, productData)}
                >
                  <FaShoppingCart />
                  Add to cart
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </body>
  );
}

export default Product;
