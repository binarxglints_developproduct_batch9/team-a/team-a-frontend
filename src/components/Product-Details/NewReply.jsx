import React, { useState } from "react";
import "../assets/css/addreply.scss";
import axios from "axios";


function NewReply(props) {
  console.log(props.id, "test id question");

  // console.log(props.item[0].data.commentator.id, "test panggil id diss");
  const [reply, setReply] = useState({
    comment: "",
  });
  // if (comment === undefined)

  const newRep = (e) => {
    e.preventDefault();

    const urlReply = `https://sayonara.kuyrek.com/discuss/${props.id}/comment`;
    if (reply.comment !== "") {
      axios
        .put(urlReply, reply)
        .then((res) => {
          console.log(res, "tes res comment");
          alert("Success adding reply!");
          window.location.reload();
        })
        .catch((err) => {
          console.log(err);
        });
    }
  };
  console.log(reply, "console isi state comments");

  return (
    <div>
      <div className="header-container border bg-light rounded p-3 rounded-lg d-flex flex-md-row">
        <div className="col d-flex flex-md-row ">
          <form onSubmit={newRep}>
            <textarea
              placeholder="write your reply here"
              className="input-add-reply"
              onChange={(e) =>
                setReply({
                  comment: e.target.value,
                })
              }
            ></textarea>

            <div className="btn-grup-new-reply flex-md-row">
              <button
                className="btn-send-new-reply background-primary"
                type="submit"
              >
                {" "}
                Send
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default NewReply;
