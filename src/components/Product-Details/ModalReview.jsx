import React, { useState, useEffect } from "react";
import { ModalBody, Modal } from "reactstrap";
import ReactStars from "react-rating-stars-component";
import { addReview } from "../../store/actions/users";
import "../assets/css/modalreview.scss";
import axios from "axios";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

function ModalReview(props) {
  let { productId } = useParams();
  console.log(productId, "test console productId test terbau wowo");
  const [reviews, setReviews] = useState({
    comment: " ",
    rating: 0,
    productId: `${productId}`,
  });
  const status = useSelector((state) => state.user);
  console.log(reviews, "state review");
  const { comment, rating } = reviews;
  const dispatch = useDispatch();
  const handleSubmit = async (e) => {
    e.preventDefault();
    dispatch(addReview({ comment, rating, productId }));
    // setTimeout(console.log('bisa plis'), 500)
    // window.location.href=`/productDetail/${productId}`
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setReviews({
      ...reviews,
      [name]: value,
    });
  };

  // useEffect(() => {
  //   if (status === 'Success add review"') {
  //     window.location.reload()
  //   }

  // }, [status])

  // const [inputRate, setRate] = useState(0);
  // const changeRate = (value) => {
  //   setRate(value)
  // }
  // console.log(inputRate)

  // let review
  // console.log(review)

  // const changeRate = (value) => {
  //   setRate(value);
  // };
  // let review

  // const addReview = (e) => {
  //   e.preventDefault();
  //   setReviews({
  //     comment: review,
  //     rating: inputRate
  //   })
  //   console.log(reviews)

  //   const urlAddReview = `https://sayonara.kuyrek.com/review/create/${productId}`;

  //   axios
  //     .post(urlAddReview, reviews)
  //     // {data: comment;
  //     // rating: 0;
  //     // }
  //     .then((res) => {
  //       console.log(res);
  //       console.log(res.data.data.id, "coba mau liat id review");
  //       localStorage.setItem("id review", res.data.data.id)
  //       alert("Success adding review!")
  //       window.location.reload()

  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  //   // console.log(dataReview, " review data");
  // };
  return (
    <body>
      <div>
        <Modal isOpen={props.modal} toggle={props.toggleModalReview}>
          <ModalBody>
            <h1 style={{ color: "#ffa45b" }}>Add Review</h1>

            <p>Rating</p>
            <ReactStars
              count={5}
              size={34}
              color2={"#ffd700"}
              name="rate"
              // onChange={changeRate}
            />

            <form className="form-review" onSubmit={handleSubmit}>
              <label>Add your Review!</label>
              <input
                className="input-review"
                type="comment"
                id="comment"
                onChange={handleChange}
                value={comment}
                name="comment"
                required
              />
              <div className="btn-grup-review">
                <button className="btn-cancel-review">Cancel</button>
                <button className="btn-add-review" type="submit">
                  Add Review
                </button>
              </div>
            </form>
          </ModalBody>
        </Modal>
      </div>
    </body>
  );
}
export default ModalReview;
