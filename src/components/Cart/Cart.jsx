import React, { useState, useEffect } from "react";
import axios from "axios";
import ModalDelete from "./ModalDeleteCart.jsx";
import "../assets/css/cart.scss";
import { Button, Modal } from "react-bootstrap";

function Cart() {
  const [modalDelete, setModalDelete] = useState(false);
  const [data, setData] = useState("");
  const toggleModalDelete = (e, id, name) => {
    console.log(id, "cek id odal delete");
    e.preventDefault();
    setModalDelete(!modalDelete);
    setData([name, id]);
  };
  const toggleSwitch = () => {
    setModalDelete(!modalDelete);
  };

  const [product, setProduct] = useState({
    productData: [],
  });
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
  }

  useEffect(() => {
    const url = `https://sayonara.kuyrek.com/cart/`;
    axios
      .get(url)
      .then((res) => {
        const resultProduct = res.data.data;
        console.log(resultProduct, "resultrpoduct");
        setProduct({
          productData: resultProduct,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const { productData } = product;
  console.log(productData, "cart");

  const [count, setCount] = useState(1);

  function add() {
    // if (count < stock)
    setCount(count + 1);
  }
  function min() {
    if (count > 0) {
      setCount(count - 1);
    }
  }

  const update = (e) => {
    setProduct({ productData: e });
  };

  let subTotal = 0;
  let totalPrice = 0;

  //   const modalDeletes =

  // function handleTotalPrice (e) {
  //    setTotalPrice(e.target.value * price.$numberDecimal)
  // }

  //   const totalPrice = count * productData[0].productId.price.$numberDecimal;

  function setCheckOut() {
    let transaction = {
      productId: [],
      quantity: [],
    };

    if (count > 0) {
      productData.map((item) => {
        const { productId } = item;
        const { id } = productId;
        transaction.productId.push(id);
        transaction.quantity.push(count);
      });

      const urlCheckout = "https://sayonara.kuyrek.com/transaction/create";

      axios
        .post(urlCheckout, transaction)
        .then((res) => {
          setError(false)
          setTitle('Pending Transaction')
          setMessage(res.data.status)
          const result = res.data.data
          setModal(true)
          window.location.href = `/payment/${result.id}/${result.total.$numberDecimal}`
        })
        .catch(
          err => {
            setError(true)
            setTitle('Error')
            if(err.message === 'Request failed with status code 401') setMessage('Please sign in first!')
            else if(err.message === 'Request failed with status code 422') setMessage(`You can't more than available stock.`)
            setModal(true)
          }
        )
    }
    else {
      setError(true)
      setTitle('Error')
      setMessage('Please input the quantity before buying')
      setModal(true)
    }
  }

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };

  return (
    <body>
      <div className="wrapper">
        <div className="card-cart">
          <div className="row">
            <div className="col-md-4 text-center">
              <span>Products</span>
            </div>
            <div className="col-md-2 text-center">
              <p>Price</p>
            </div>
            <div className="col-md-2 text-center">
              <p className="ml-5">Quantity</p>
            </div>
            <div className="col-md-2 text-center">
              <p>Total Price</p>
            </div>
            <div className="col-md-2 text-center">
              <p>Action</p>
            </div>
          </div>
          {productData
            ? productData.map((item) => {
                const { productId, _id } = item;
                console.log(item, "cek item mapping");
                const { image, price } = productId;
                return (
                  <div className="content-card row h-40" key={_id}>
                    <div className="col-md-2">
                      <img
                        src={`https://sayonara.kuyrek.com/${image}`}
                        alt="product"
                        className="img-cart d-inline"
                      />
                    </div>
                    <div className="col-md-2 mt-5">
                      <p>{item.productId.name}</p>
                    </div>

                    <div className="col-md-2 text-center mt-5">
                      <p>Rp {priceForm(price.$numberDecimal)}</p>
                    </div>
                    <div className="col-md-2 d-flex flex-wrap justify-content-center mt-5">
                      <button
                        className="btn-qty rounded-left h-25"
                        onClick={add}
                      >
                        +
                      </button>
                      {/* <p className="count mx-3 mt-2">{count}</p> */}
                      <input
                        className="count w-25 text-center h-25"
                        type="text"
                        value={count}
                        onChange={priceForm(
                          (totalPrice = count * price.$numberDecimal),
                          (subTotal += totalPrice)
                        )}
                        disabled
                      />
                      <button
                        className="btn-qty rounded-right h-25"
                        onClick={min}
                      >
                        -
                      </button>
                    </div>
                    <div className="col-md-2 text-center mt-5">
                      {console.log(totalPrice, "total price")}
                      <p> Rp {priceForm(totalPrice)}</p>
                    </div>
                    <div className="col-md-1 mt-5">
                      <button
                        className="btn-action-cart ml-3"
                        onClick={(e) =>
                          toggleModalDelete(e, _id, item.productId.name, update)
                        }
                      >
                        DELETE
                      </button>
                      {console.log(item.productId.name, "ini namaaa")}
                    </div>
                  </div>
                );
              })
            : null}
          <ModalDelete
            modal={modalDelete}
            toggleModalDelete={toggleModalDelete}
            toggleSwitch={toggleSwitch}
            data={data}
            // id={id}
            datas={product}
            setData={update}
            // name={item.productId.name}
          />

          <div className="subtotal-cart row">
            <div className="col-md-8 text-right">
              <p>Sub Total:</p>
            </div>
            <div className="col-md-2 text-center">
              <b>
                <p style={{ color: "#ffa45b" }}> Rp {priceForm(subTotal)}</p>
              </b>
            </div>
            <div className="col-md-2 text-center">
              {/* <Link to="/payment"> */}
              <button className="btn-checkout-cart" onClick={setCheckOut}>
                Checkout
              </button>
              {/* </Link> */}
            </div>
          </div>
        </div>
      </div>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </body>
  );
}

export default Cart;
