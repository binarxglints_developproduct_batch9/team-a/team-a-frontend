import axios from "axios";
import React, { useState } from "react";
import { Modal, ModalBody } from "reactstrap";
import "../assets/css/modaldeletecart.scss";

function ModalDelete(props) {
  const [show, setShow] = useState(false);
  console.log(props.id, "props");
  const passing = props.datas;
  console.log(passing, "cek passing");

  const deleteCart = (e) => {
    e.preventDefault();
    console.log(e, props.data[0], props.data[1], "cek e modal");
    const urlDelete = `https://sayonara.kuyrek.com/cart/delete/${props.data[1]}`;
    const cek = passing.productData.filter(
      (item) => item._id !== props.data[1]
      // console.log(item, "cek item");
      // const { id } = item
    );
    console.log(cek, "cek nih bos");
    props.setData(cek);

    axios
      .delete(urlDelete)
      .then((res) => {
        window.location.reload()
      })
      .catch((err) => {
        console.log(err);
      });
  };

  return (
    <body>
      <div>
        <Modal isOpen={props.modal} toggle={props.toggleModalDelete}>
          <ModalBody>
            {/* <img src={img} alt="danger" className="img-danger"/> */}
            <div className="contet-modal-delete-cart">
              <h3>Delete this cart?</h3>
              <p>This action cannot be undone</p>
            </div>
            <div className="btn-group-delete-cart">
              <button className="btn-delete-modal-cart" onClick={deleteCart}>
                Yes, delete it!
              </button>
              <button className="btn-cancel-modal-cart">Cancel</button>
            </div>
          </ModalBody>
        </Modal>
      </div>
    </body>
  );
}

export default ModalDelete;
