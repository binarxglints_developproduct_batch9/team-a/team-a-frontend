import React from 'react';
import "bootstrap/dist/css/bootstrap.css";
import Logo from "../assets/images/Sayonara.png";
import { Link } from 'react-router-dom';

function SignUp() {
  return (
    <div>
      <main className="justify-content-center align-content-center mt-5">
        <div className="container p-0">
          <div className="row">
            <div className="col-md-7 my-auto text-center">
              <div className="col">
                <Link to="/"><img className="img-fluid" style={{width:"600px"}} src={Logo} alt="" /></Link>
              </div>
            </div>
            <div className="col px-4 py-2 mx-5 my-1 border bg-light">
              <form className="px-6">
                <h6 className="text-center">Sign Up</h6>
                <div className="form-group">
                  <label for="fullname">Full Name</label>
                  <input type="text" className="form-control form-control-sm" id="fullname" required />
                </div>
                <div className="form-group">
                  <label for="email">Email</label>
                  <input type="email" className="form-control form-control-sm" id="email" required />
                </div>
                <div className="form-group">
                  <label for="password">Password</label>
                  <input type="password" className="form-control form-control-sm" id="password" required />
                </div>
                <div className="form-group">
                  <label for="confirm-password">Confirm Password</label>
                  <input type="password" className="form-control form-control-sm" id="confirm-password" required />
                </div>
                <button type="submit" className="btn btn-success btn-lg btn-block color-light background-primary outline-light">Submit</button>
                <p className="text-center pt-2">Already have account? <a href="/sign-in" className="color-primary text-decoration-none"> Sign In </a></p>
              </form>
            </div>
          </div>
        </div>
      
      </main>
      <footer>
        <div className="container mt-4 border-top">
          <div className="row">
            <div className="col">
              <p>2020, SayurHub</p>
            </div>
          </div>
        </div>
      </footer> 
    </div>

  )
}

export default SignUp
