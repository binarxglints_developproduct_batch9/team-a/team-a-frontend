import React, { useState, useEffect } from "react";
import { Button, Carousel, Modal } from "react-bootstrap";
import axios from "axios";
import { FaShoppingCart } from "react-icons/fa";
import "../assets/css/Carousel.css";
import { Link } from "react-router-dom";
import sale from "../assets/images/SALE OF THE YEAR.png";
import banner from "../assets/images/iklan.jpg"


function Carousels() {
  const [data, setData] = useState({
    productData: [],
  })
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
  }

  useEffect(() => {
    const url = "https://sayonara.kuyrek.com/home/page/carousel";
    axios
      .get(url)
      .then((res) => {
        const resultProduct = res.data.data;
        setData({
          productData: resultProduct,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const { productData } = data;
  console.log(productData, "product detail");

  const addToCart = (add, item) => {
    const urlCart = "https://sayonara.kuyrek.com/cart/create"

    console.log(item, "item")
    axios
      .post(urlCart, {
        productId: item.id,
      })
      .then((res) => {
        setError(false)
        setTitle('Success')
        setMessage('Item added to cart.')
        setModal(true)
      })
      .catch((err) => {
        setError(err.message)
        setTitle('Error')
        if(err.message === "Request failed with status code 401") setMessage("Please Sign In first!")
        else if(err.message === "Request failed with status code 422") setMessage("You can't buy your own product!")
        setModal(true)
      })
  };

  return (
    <div className="carousel-cointainer">
      <div>
        <img src={banner} alt="banner" className="banner" />
      </div>
      <div>
        <hr className="hr" />
        <h3 className="color-primary text-center">
          <b>Today's Highlight</b>
        </h3>
        <h5 className="color-accent text-center">All item only Rp 10,000!</h5>
        <Carousel>
          {productData.map((item) => {
            const { id, image } = item;

            return (
              <Carousel.Item key={id}>
                <Link to={`/productDetail/${id}`}>
                  <img
                    className="d-block images-carousel"
                    src={`https://sayonara.kuyrek.com${image}`}
                    alt="First slide"
                  />
                  <span className="badge test-badge">Rp 10,000,-</span>
                </Link>
                <button
                  type="button"
                  className="btn-carousel color-light background-primary outline-light"
                  onClick={(add) => addToCart(add, item)}
                >
                  <FaShoppingCart /> Add to cart
                </button>
              </Carousel.Item>
            );
          })}
        </Carousel>
        <div>
          <img src={sale} alt="sale" className="ads-img" />
        </div>
        <div className="newsletter">
          <h3 className="text-center color-primary">
            <b>Subscribe to our newsletter</b>
          </h3>
          <form>
            <label className="header-container">
              <p className="news" >Name</p>
              <input type="text" className="input-newsletter"></input>
              <p className="news" >Email</p>
              <input type="text" className="input-newsletter"></input>
              <p className="newss" >Phone Number</p>
              <input type="text" className="input-newsletter"></input>
              <button
                type="submit"
                className="btn-newsletter background-accent"
              >
                Submit now!
              </button>
            </label>
          </form>
        </div>
        <hr className="hr" />
      </div>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Carousels;
