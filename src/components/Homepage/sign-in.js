import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux'
// import { data } from 'jquery';
import "bootstrap/dist/css/bootstrap.css";
import Logo from "../assets/images/Sayonara.png";
import vector from "../assets/images/vector.png";

import {
  GET_SIGNIN
} from '../../redux/actions/todo-action'
import { Link } from 'react-router-dom';



const SignIn = ({
  // todo: { loading, todos },
  getSignIn
}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const toggle = (e) => {
    e.preventDefault()
    const data = {emailOrUsername:email, password:password}
    getSignIn(data)
    localStorage.setItem('token', 'token')
  } 
  // useEffect
  console.log(email, password)
  return (
    <div>
          <header>
        <nav className="navbar navbar-light py-3 ">
          
        </nav>
      </header>
      
      <main>
        <div className="container p-0">
          <div className="row">
            <div className="col-md-8 my-auto text-center">
              <div className="col">
                <Link to="/"><img className="img-fluid" style={{width:"600px"}} src={Logo} alt="" /></Link>
              </div>
            </div>
            <div className="col px-4 py-2 mx-4 my-1 border bg-light">
              {/* <form className="px-6"> */}
                <h4 className="text-center fs-1">Sign In</h4>
                <div className="form-group">
                  <label for="email">Email</label>
                  <input type="text" onChange={e => setEmail(e.target.value)} value={email} className="form-control form-control-sm" id="email" required />
                </div>
                <div className="form-group">
                  <label for="password">Password</label>
                  <input type="password" onChange={e => setPassword(e.target.value)} value={password} className="form-control form-control-sm" id="password" required />
                  <p className="text-center pt-2"><a href="" className="text-decoration-none color-primary"> Forgot Password </a></p>
                </div>
                <button onClick={(e) => toggle(e)} className="btn color-light background-primary outline-light btn-lg btn-block">SIGN IN</button>
                <p className="text-center pt-2">Dont't have an account? <a href="/sign-up" className="text-decoration-none color-primary"> Sign Up </a></p>
              {/* </form> */}
            </div>
          </div>
        </div>
      
      </main>
      <footer>
        <div className="container mt-4 border-top">
          <div className="row">
            <div className="col">
              <p>2020, SayurHub</p>
            </div>
          </div>
        </div>
      </footer> 
    </div>

  )
}

const mapStateToProps = (state) => ({
  todo: state.todo
})

// Get dispatch / function to props
const mapDispatchToProps = (dispatch) => ({
  getSignIn: (id) => dispatch({ type: GET_SIGNIN, payload: id}),
})

// To make those two function works register it using connect
export default connect(mapStateToProps, mapDispatchToProps)(SignIn)
