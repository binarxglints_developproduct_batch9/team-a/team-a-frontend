import React from "react";
import { Col, Image, Row } from "react-bootstrap";
import {
  FaFacebookF,
  FaInstagram,
  FaTwitter,
  FaYoutube,
  FaGooglePlay,
  FaAppStore,
  FaRegCopyright,
} from "react-icons/fa";
import { Link } from "react-router-dom";
import logo from "../assets/images/Sayonara.png";
import "../assets/css/footer.scss";
import footer from "../assets/images/BINAR-removebg-preview.png";

const Footer = () => {
  return (
    <div className="header-container background-secondary p-4 mb-2 h4">
      <Row>
        <Col md="5">
          <Image src={logo} className="w-75 h-auto" />
        </Col>
        <Col>
          <ul className="list-unstyled mt-2">
            <li className="mb-2">
              <b>About Us</b>
            </li>
            <p>
              <small>
                Sayonara is created by Team A for learning purposed as a final
                project of Glints Academy Batch #9
              </small>
            </p>
            <button className="mt-2 btn-contact">Contact</button>
          </ul>
        </Col>
        <hr className="hr-line" />
        <Col className="text-center">
          {/* <p>Social Media</p> */}
          <FaFacebookF size="2rem" className="m-3" />
          <FaTwitter size="2rem" className="m-3" />
          <FaYoutube size="2rem" className="m-3" />

          <FaInstagram size="2rem" className="m-3" />

          <vl className="vl-line" />
          <span>
            <small>Available on:</small>
          </span>
          <FaGooglePlay size="2rem" className="m-3" />
          <FaAppStore size="2rem" className="m-3" />
          <vl className="vl-line" />
          <img src={footer} alt="footer" className="image-footer ml-5" />
          <p className="text-left">
            <small>&copy;Copyright 2021 Sayonara All Rights Reserved</small>
          </p>
        </Col>
      </Row>
    </div>
  );
};

export default Footer;
