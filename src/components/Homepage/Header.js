import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Form,
  FormControl,
  Image,
  InputGroup,
  Modal,
  Nav,
  Navbar,
  NavDropdown,
} from "react-bootstrap";
import { FaSearch, FaShoppingCart } from "react-icons/fa";
import ModalCreateProduct from "../modaluser/ModalCreateProduct";
import logo from "../assets/images/Sayonara.png";
import axios from "axios";

const Header = () => {
  let searchTerm = "";
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === true) window.location.href = '/signin'
  }

  const handleSearch = () => {
    localStorage.setItem("searchTerm", searchTerm);
  };

  const handleReset = () => {
    localStorage.removeItem("searchTerm");
  };

  const handleSignOut = () => {
    localStorage.removeItem("token")
    localStorage.removeItem("searchTerm")
    localStorage.removeItem('username')
    localStorage.removeItem('id product')
    localStorage.removeItem('id review')
    localStorage.removeItem('subtotal')
    localStorage.removeItem('stock')
    localStorage.removeItem('uploaded image')
  };

  const [image, setImage] = useState('')

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/profile`)
      .then(
        res => {
          const result = res.data.data.image
          setImage(result)
        }
      )
      .catch(
        err => {
          if(image !== '') {
            if(err.message === 'Request failed with status code 401') {
              setError(true)
              setTitle('Error')
              setMessage('Please sign in first!')
            }
          }
        }
      )
  }, [])

  

  // const handleLoggedIn = () => {[
  //   localStorage.setItem('token', 'qwerty')]
  //   window.location.reload()
  // }

  const [modalCreateProduct, setModalCreateProduct] = useState(false);
  const toggleModalCreateProduct = () =>
    setModalCreateProduct(!modalCreateProduct);
  const toggleSwitch = () => {
    setModalCreateProduct(!modalCreateProduct);
  };
  return (
    <div className="header-container sticky-top">
      <Navbar bg="white" expand="xl">
        <Navbar.Brand href="/" onClick={handleReset} className="m-0">
          <Image style={{ width: "15rem", height: "auto" }} src={logo} />
        </Navbar.Brand>

        <Navbar.Toggle aria-controls="basic-navbar-nav" />

        <Navbar.Collapse id="basic-navbar-nav" className="text-center">
          <Col>
            <Form inline onSubmit={handleSearch} action="/search">
              <InputGroup className="mx-auto">
                <InputGroup.Prepend>
                  <InputGroup.Text className="bg-white">
                    <FaSearch color="#ffa45b" />
                  </InputGroup.Text>
                </InputGroup.Prepend>
                <FormControl
                  type="text"
                  placeholder="Search Product"
                  style={{ width: 450 }}
                  onChange={(e) => (searchTerm = e.target.value)}
                />
              </InputGroup>
            </Form>
          </Col>
          <Nav>
            {image !== '' ? (
              <>
                <NavDropdown
                  className="my-1 mr-2"
                  title={<Image style={{width: '2.35rem', height: '2.35rem'}} src={`https://sayonara.kuyrek.com${image}`} roundedCircle />}
                  id="basic-nav-dropdown"
                >
                  <NavDropdown.Item className="text-center">
                    Hi, {localStorage.getItem('username')}
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/profilepage" className="text-center">
                    Profile
                  </NavDropdown.Item>
                  <NavDropdown.Item href="/change_password" className="text-center">
                    Change Password
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item
                    href="/"
                    onClick={handleSignOut}
                    className="mr-5 text-center"
                  >
                    Sign Out
                  </NavDropdown.Item>
                </NavDropdown>
                <Nav.Link href="/cart" className="my-1">
                  <FaShoppingCart size="25" />
                </Nav.Link>

                <Button
                  className="color-primary background-light outline-primary sell-product m-2"
                  onClick={toggleModalCreateProduct}
                >
                  Sell Product
                </Button>
                <ModalCreateProduct
                  modal={modalCreateProduct}
                  toggleModalCreateProduct={toggleModalCreateProduct}
                  toggleSwitch={toggleSwitch}
                />
              </>
            ) : (
              <>
                <Nav.Link href="/signup" className="mx-1 px-0">
                  <Button className="color-light background-primary outline-light">
                    Sign Up
                  </Button>
                </Nav.Link>

                <Nav.Link href="/signin" className="mx-1 mr-5 px-0">
                  <Button className="color-primary background-light outline-light">
                    Sign In
                  </Button>
                </Nav.Link>
              </>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Header;
