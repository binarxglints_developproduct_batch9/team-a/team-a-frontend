import React, { useState } from "react";
import { FaShoppingCart } from "react-icons/fa";
import axios from "axios"
import "bootstrap/dist/css/bootstrap.css";
import "../assets/css/tes.css";
import { Link } from "react-router-dom";
import { Button, Modal } from "react-bootstrap";

function ProductList(props) {
  const { category, loading, products, page, totalePage, getMore } = props;

  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
  }

  //https://codepen.io/malasngoding/pen/EedMvv
  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };


  const addToCart = (add, item) => {
    const urlCart = "https://sayonara.kuyrek.com/cart/create";
    
    console.log(item, "item")
    axios.post(urlCart, {
      productId: item.id
    })
    .then((res) => {
      setError(false)
      setTitle('Success')
      setMessage('Item added to cart.')
      setModal(true)
    })
    .catch(
      err => {
        setError(err.message)
        setTitle('Error')
        if(err.message === 'Request failed with status code 401') setMessage('Please Sign In first!')
        else if(err.message === 'Request failed with status code 422') setMessage(`You can't buy your own product!`)
        setModal(true)
      }
    )

  }

  return (
    <div class="cardcontain">
      {console.log(products, "tes")}
      {products.length || !loading
        ? products.map((item) => (
            <div class="cards" key={item.id}>
              <Link className="text-decoration-none" to={`/productDetail/${item._id}`}>
                <img class="imgcards" src={`https://sayonara.kuyrek.com${item.image}`} alt="product" />
              </Link>
              <div class="txtbod">
                <Link className="text-decoration-none" to={`/productDetail/${item._id}`}>
                  <h1 class="item-name">{item.name}</h1>
                  <p class="price">
                    Rp.{" "}
                    {item.actualPrice
                      ? priceForm(item.actualPrice)
                      : priceForm(item.price.$numberDecimal)}
                    ,-
                  </p>
                </Link>
                <div class="btnposs">
                  <Button className="btnadd" onClick={(add) => addToCart(add, item)}>
                    <FaShoppingCart />
                    Add to Cart
                  </Button>
                </div>
              </div>
            </div>
          ))
        : null}
      {!products.length && "There isn't any product in this category"}
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ProductList;
