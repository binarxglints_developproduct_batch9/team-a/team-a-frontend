import React, { useState, useEffect } from "react";
import axios from "axios";
import ProductList from "./ProductList";
import "../assets/css/tes.css";

function ButtonCategory() {
  const [category, setCat] = useState("all"),
    [loading, setLoading] = useState(false),
    [page, setPage] = useState(1),
    [totalPage, setTotal] = useState(0),
    [showScroll, setShowScroll] = useState(false),
    [products, setProducts] = useState([]);

  useEffect(() => {
    const getProduct = async (cat) => {
      setLoading(true);
      try {
        const prods = await axios.get(
          cat === "all"
            ? `https://sayonara.kuyrek.com/home/`
            : `https://sayonara.kuyrek.com/home/category/${cat}`
        );
        // category
        category === "fruit"
          ? setProducts(prods.data.data)
          : category === "vegetable"
          ? setProducts(prods.data.data)
          // : category === "diets"
          // ? setProducts(prods.data.data)
          : setProducts(prods.data.data);
        setTotal(prods.data.data.totalPages);
        setPage(1);
        setLoading(false);
      } catch (error) {
        console.log("ada error di:", error);
      }
    };
    getProduct(category);
  }, [category]);

  return (
    <div className="btncat-container">
      <div>
        <button
          className={`btncat ${category === "all" }`} 
          onClick={() => setCat("all")}
          id="button"
        >
          All
        </button>
        <button
          className={`btncat ${category === "fruit" }`}
          onClick={() => setCat("fruit")}
          id="button"
        >
          Fruits
        </button>
        <button
          className={`btncat ${category === "vegetable" }`}
          onClick={() => setCat("vegetable")}
          id="button"
        >
          Vegetables
        </button>
      </div>
      <div>
        <ProductList
          category={category}
          loading={loading}
          products={products}
          page={page}
          totalPage={totalPage}
        />
      </div>
    </div>
  );
}
export default ButtonCategory;
