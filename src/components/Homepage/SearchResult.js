import React, { useEffect, useState } from 'react'
import axios from 'axios'
import { Button, Modal } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import { FaShoppingCart } from 'react-icons/fa'

const SearchResult = () => {
  const [data, setData] = useState({
    productData: []
  })
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
  }

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/home/find/${localStorage.getItem('searchTerm')}`)
      .then(
        res => {
          const result = res.data.data
          setData({
            productData: result
          })
        }
      ).catch(
        err => {
          console.log(err)
        }
      )
  }, [])

  const { productData } = data
  console.log(productData, 'product data')

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  const addToCart = (e, id) => {
    console.log(id)

    axios.post(`https://sayonara.kuyrek.com/cart/create`, { productId: id })
      .then(
        res => {
          setError(false)
          setTitle('Success')
          setMessage('Item added to cart.')
          setModal(true)
        }
      )
      .catch(
        err => {
          setError(err.message)
          setTitle('Error')
          if(err.message === 'Request failed with status code 401') setMessage('Please sign in first!')
          else if(err.message === 'Request failed with status code 422') setMessage(`You can't buy your own product!`)
          setModal(true)
        }
      )
  }

  return (
    <>
    <div className="header-container text-center mb-0"><h6>Search result of the query "{localStorage.getItem('searchTerm')}"</h6></div>
    <div className="header-container d-flex flex-wrap justify-content-around mt-0">
      {
        productData
        ? productData.map(item => {
          return (
            <div class="cards" key={item.id}>
              <Link className="text-decoration-none" to={`/productDetail/${item._id}`}>
                <img class="imgcards" src={`https://sayonara.kuyrek.com${item.image}`} alt="product" />
              </Link>
              <div class="txtbod">
                <Link className="text-decoration-none" to={`/productDetail/${item._id}`}>
                  <h1 class="item-name">{item.name}</h1>
                  <p class="price">
                    Rp.{" "}
                    {item.actualPrice
                      ? priceForm(item.actualPrice)
                      : priceForm(item.price.$numberDecimal)}
                    ,-
                  </p>
                </Link>
                <div class="btnposs">
                  <Button className="btnadd" onClick={(add) => addToCart(add, item)}>
                    <FaShoppingCart />
                    Add to Cart
                  </Button>
                </div>
              </div>
            </div>
          )
        })
        : null
      }
      {!productData.length && "There isn't any product related to your query"}
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
    </>
  )
}

export default SearchResult
