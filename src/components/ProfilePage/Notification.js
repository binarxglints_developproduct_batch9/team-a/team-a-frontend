import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Col, Image, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Notification = () => {
  const [data, setData] = useState({})
  
  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/transaction/seller/notif`)
      .then(
        res => {
          const result = res.data.data
          setData({result})
        }
      ).catch(
        err => console.log(err)
      )
  }, [])

  const { result } = data
  console.log(result, 'notification')

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <>
      {
        result
        ? result.map(item => {
          const { id, productId, total, user } = item
          return (
            <div key={id} className="header-container mb-3 bg-white rounded p-3 border rounded-lg">
              <Row>
                <Col>
                  <h6>Transaction No. {id}</h6>
                </Col>
              </Row>

              <Row>
                <Col md="9">
                  {
                    productId.map((product, index) => {
                      const { image, name, price, quantity, seller } = product
                      return (
                        <Row className="mt-2">
                          <Col md="4">
                            <Image 
                              className="mb-3 border h-auto"
                              key={index}
                              src={`https://sayonara.kuyrek.com/${image}`} 
                              rounded 
                            />
                          </Col>

                          <Col>
                            <p>Product: {name}</p>
                            <p>Price: Rp {priceForm(price.$numberDecimal)} / kg</p>
                            <p>Quantity: {quantity} kg</p>
                          </Col>
                        </Row>
                      )
                    })
                  }
                </Col>

                <Col md="3" className="text-center">
                  <p>Total Price</p>
                  <p>Rp {priceForm(total.$numberDecimal)}</p>
                  <p>{ user.username } has purchased this product</p>
                  <Link to={`/sellerConfirm/${id}`}>
                    <Button className="color-primary outline-primary background-light mb-3">
                      Confirm
                    </Button>
                  </Link>
                </Col>
              </Row>
            </div>
          )
        })
        : null
      }
      { result === undefined ? null : !result.length && <p className="mt-5 ml-5">There is no notifcation</p> }
    </>
  )
}

export default Notification
