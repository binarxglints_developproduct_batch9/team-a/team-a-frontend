import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Col, Image, Row } from 'react-bootstrap'

const SellerTransaction = () => {
  const [data, setData] = useState({})
  
  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/transaction/seller`)
      .then(
        res => {
          const result = res.data.data
          setData({result})
        }
      ).catch(
        err => console.log(err)
      )
  }, [])

  const { result } = data
  console.log(result, 'seller data')

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  }

  return (
    <>
      {
        result
        ? result.map(item => {
          const { id, productId, status, total, user } = item
          return (
            <div key={id} className="header-container mb-3 bg-white rounded p-3 border rounded-lg">
              <Row>
                <Col>
                  <h6>Transaction No. {id} by {user.username}</h6>
                </Col>
              </Row>
              
              <Row>
                <Col md="9">
                  {
                    productId.map((product, index) => {
                      const { image, name, price, quantity, seller } = product
                      if(seller.username === localStorage.getItem('username')) {
                        return (
                          <Row className="mt-2">
                            <Col md="4">
                              <Image 
                                className="mb-3 border h-auto"
                                key={index}
                                src={`https://sayonara.kuyrek.com${image}`} 
                                rounded 
                              />
                            </Col>
  
                            <Col>
                              <p>Product: {name}</p>
                              <p>Price: Rp {priceForm(price.$numberDecimal)} / kg</p>
                              <p>Quantity: {quantity} kg</p>
                            </Col>
                          </Row>
                        )
                      }
                    })
                  }
                </Col>

                <Col md="3" className="text-center">
                  <p>Transaction Status</p>
                    {
                      status === 'pending'
                      ? <p className="text-danger">
                        PENDING<br />
                        WAITING FOR PAYMENT<br />
                      </p>
                      : status === 'success'
                        ? <p style={{color: "green"}}>SUCCESS</p>
                        : status === 'on process'
                          ? <p className="color-primary">ON PROCESS</p>
                          : status === 'canceled'
                            ? <p className="text-danger">CANCELED</p>
                            : null
                    }
                  <p>Total Price<br />Rp {priceForm(total.$numberDecimal)}</p>
                </Col>
              </Row>
            </div>
          )
        })
        : null
      }
      { result === undefined ? null : !result.length && <p className="mt-5 ml-5">There is no sell history</p> }
    </>
  )
}

export default SellerTransaction
