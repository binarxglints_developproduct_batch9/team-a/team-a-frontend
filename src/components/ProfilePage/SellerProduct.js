import React, { useEffect, useState } from "react";
import image from "../assets/images/banana.jpeg";
import axios from "axios";
import "../assets/css/SellerProduct.scss";
import ModalDeleteProduct from "../modaluser/ModalDeleteProduct";
import ModalEditProduct from "../modaluser/ModalEditProduct";
import { Card } from "react-bootstrap";
function SellerProduct(props) {
  const [product, setProduct] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");
  console.log(props, "midori");

  useEffect(() => {
    axios
      .get(`https://sayonara.kuyrek.com/home/seller/${localStorage.getItem(
        "username"
      )}
  `)
      .then((res) => {
        setProduct(res.data.data);
        setLoad(true);
        {
          console.log(res.data.data);
        }
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };

  return (
    <div>
      <div class="scss-sellerproduct">
        {console.log(product.length, "tes")}

        {product.length
          ? product.map((item) => (
              <div class="cardsize" key={item._id}>
                <Card style={{ width: "20rem" }}>
                  <Card.Img
                    class="img-size"
                    src={`https://sayonara.kuyrek.com${item.image}`}
                    alt="product"

                  />
                  <Card.Body className="boddy">
                    <h1 class="item-name">{item.name}</h1>
                    <p class="price">
                      Rp. {priceForm(item.price.$numberDecimal)}
                      ,-
                    </p>
                    <div class="btnpos">
                      <ModalEditProduct item={item} />
                      <ModalDeleteProduct item={item} />
                    </div>
                  </Card.Body>
                </Card>
              </div>
            ))
            : null}
        {!product.length && "There isn't any product in this category"}
      </div>
    </div>
  );
}
export default SellerProduct;
