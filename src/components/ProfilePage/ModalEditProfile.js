import { Button, Modal } from "react-bootstrap";
import React, { useState } from "react";
import axios from "axios";
import "../assets/css/ModalEditProfile.css";

function ModalEditProfile(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === false) window.location.reload()
    else if(error === 'Request failed with status code 401') window.location.href = '/signin'
  }
  
  let username = props.users.username;
  let fullname = props.users.fullName;

  const [file, setFile] = useState({
    selectedFile: null,
  });

  const handleFile = (e) => {
    setFile({
      selectedFile: e.target.files[0],
    });
  };

  const handleUpload = (e) => {
    e.preventDefault();

    const fd = new FormData();

    fd.append("username", username)
    fd.append("fullName", fullname)
    // fd.append("image", file.selectedFile, file.selectedFile.name)
    if(file.selectedFile !== null) fd.append("image", file.selectedFile, file.selectedFile.name)

    axios
      .put(`https://sayonara.kuyrek.com/profile/update`, fd)
      .then((res) => {
        setError(false)
        setTitle('Success')
        setMessage('Update data success')
        localStorage.setItem('username', username)
        setShow(false)
        setModal(true)
      })
      .catch(
        err => {
          setError(err.message)
          setTitle('Error')
          if(err.message === 'Request failed with status code 401') setMessage('Please Sign In first!')
          else if(err.message === 'Request failed with status code 422') setMessage(`Please fill the empty field.`)
          setModal(true)
        }
      )
  };
  const [picture, setPicture] = useState(null);
  const [img, setImg] = useState(null);

  const onChangePicture = (e) => {
    if (e.target.files[0]) {
      setPicture(e.target.files[0]);
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setImg(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <div>
      <Button className="editbtn" onClick={handleShow}>
        EDIT
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Profile</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleUpload} id="editprform">
            <img className="preview" src={img} />
            <input
              accept="image/png, image/jpeg, image/jpg"
              type="file"
              id="actual-btn"
              onChange={(e) => {
                onChangePicture(e);
                handleFile(e);
              }}
              hidden
            />
            <label className="label" for="actual-btn">
              UPLOAD IMAGE
            </label>
            <p>Username</p>
            <input
              type="text"
              onChange={(e) => (username = e.target.value)}
              className="proinput"
              defaultValue={username}
            ></input>
            <p>Fullname</p>
            <input
              type="text"
              onChange={(e) => (fullname = e.target.value)}
              className="proinput"
              defaultValue={fullname}
            ></input>
            <Button type="submit" className="saveedt">
              SAVE CHANGES
            </Button>
          </form>
        </Modal.Body>
      </Modal>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default ModalEditProfile;
