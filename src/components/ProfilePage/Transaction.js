import React, { useState } from 'react'
import { Tab, Tabs } from 'react-bootstrap'
import SellerTransaction from './SellerTransaction'
import BuyerTransaction from './BuyerTransaction'

const Transaction = () => {
  const [key, setKey] = useState("")
  return (
    <div>
      <Tabs
        variant="pills"
        tabClassName="tabssett"
        id="controlled-tab-example"
        className="mt-3 ml-2"
        activeKey={key}
        onSelect={(k) => setKey(k)}
      >
        <Tab 
          tabClassName="tabbtn"
          eventKey="sell" 
          title="Sell"
        >
          <SellerTransaction />
        </Tab>
            
        <Tab
          tabClassName="tabbtn"
          eventKey="buy"
          title="Buy"
        >
          <BuyerTransaction />
        </Tab>
      </Tabs>
    </div>
  )
}

export default Transaction
