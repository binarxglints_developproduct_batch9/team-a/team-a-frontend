import React, { useEffect, useState } from 'react'
import "../assets/css/sellerpage.scss"
import StarRatingComponent from "react-star-rating-component";
import "../assets/css/UserProfile.scss";
import { Card, Button, Image } from "react-bootstrap";
import ModalEditProfile from "../ProfilePage/ModalEditProfile";
import axios from "axios";


function SellerPage(props) {
  const [users, setUsers] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");

  useEffect(() => {
    axios
      .get(
        `https://sayonara.kuyrek.com/profile/${props.seller}`
      )
      .then((rest) => {
        setUsers(rest.data.data);
        setLoad(true);
        {
          console.log(rest.data.data, "hijau");
        }
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  return (
    <div>
      {console.log(users, "tess")}

      <Card key={users.id} className="carduser">
        <Image
          className="imguser"
          variant="top"
          src={`http://122.248.229.2:3003/${users.image}`}
          alt="pic"
          roundedCircle
        />
        <div className="usernameuser">
          <h1>{users.username}</h1>
          <StarRatingComponent
            editing={false}
            starCount={5}
            value={1}
            className="starrates"
          />
        </div>
      </Card>
    </div>
  );
}

export default SellerPage
