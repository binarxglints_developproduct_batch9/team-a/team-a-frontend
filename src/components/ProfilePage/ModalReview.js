import axios from 'axios';
import React, { useState } from 'react'
import { Button, Form, Image, Modal } from 'react-bootstrap';
import ReactStars from 'react-rating-stars-component';

const ModalReview = (props) => {
  const [rating, setRating] = useState(0)
  const [review, setReview] = useState('')
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleRating = rating => setRating(rating)
  const handleReview = e => setReview(e.target.value)
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
    else if(error === false) window.location.href = '/profilepage'
  }
  
  const { product } = props
  const { image, name, _id } = product

  const handleSubmit = e => {
    e.preventDefault()

    const data = {
      comment: review,
      rating: rating
    }
    console.log(data)
    
    axios.post(`https://sayonara.kuyrek.com/review/create/${_id}`, data)
    .then(
      res => {
        setError(false)
        setTitle('Success')
        setMessage(res.data.status)
        setShow(false)
        setModal(true)
      }
    )
    .catch(
      err => {
        console.log(err.message)
        setError(err.message)
          setTitle('Error')
          if(err.message === 'Request failed with status code 401') setMessage('Please Sign In first!')
          else if(err.message === 'Request failed with status code 422') setMessage(`Please input the empty field.`)
          setModal(true)
      }
    )
  }

  return (
    <>
      <Button
        className="color-primary outline-primary background-light mb-3" 
        onClick={handleShow}
      >
        Review
      </Button>

      
      <Modal show={show} onHide={handleClose}>
        <Form onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>
              Review {name}
            </Modal.Title>
          </Modal.Header>
          
          <Modal.Body className="text-center">
          
            <Image
              style={{width: "10rem", height: "10rem"}}
              src={`https://sayonara.kuyrek.com${image}`}
            />
            <ReactStars
              count={5}
              onChange={handleRating}
              size={24}
              activeColor="#ffd700"
            />
            <Form.Control onChange={handleReview} />
          </Modal.Body>
          
          <Modal.Footer>
            <Button className="color-primary background-light outline-primary" type="submit">
              Add Review
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default ModalReview
