import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Col, Form, Image, Modal, Row } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'
import { FaArrowLeft } from 'react-icons/fa'

const SellerConfirm = () => {
  const [resi, setResi] = useState('')
  const [data, setData] = useState({})
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === "Request failed with status code 401") window.location.href = '/signin'
    window.location.href = '/profilepage'
  }

  const transactionId = useParams().transactionId

  const handleSubmit = e => {
    e.preventDefault()

    console.log(resi, 'receipt')

    axios.put(`https://sayonara.kuyrek.com/transaction/update/resi/${transactionId}`)
      .then(
        res => {
          setError(false)
          setTitle('Success')
          setMessage('Receipt number submitted')
          setModal(true)
        }
      )
      .catch(
        err => {
          setError(err.message)
          setTitle('Error')
          if(err.message === 'Request failed with status code 401') setMessage('Please Sign In first!')
          else if(err.message === 'Request failed with status code 422') setMessage('Please input the receipt number.')
          setModal(true)
        }
      )
  }

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/transaction/seller/notif`)
    .then(
      res => {
        const result = res.data.data
        setData({ result })
      }
    )
    .catch(
      err => console.log(err)
    )
  }, [])
  
  const { result } = data
  console.log(result, 'seller confirm data')

  return (
    <div className="header-container bg-white text-center p-5 d-flex flex-wrap justify-content-center">
      {
        result
        ? result.map(item => {
          const { id } = item
          if(transactionId === id) {
            return (
              <div key={id}>
                <Row className="text-left mb-3">
                  <Link to="/profilepage">
                    <FaArrowLeft style={{fontSize: "1.6rem", color: "black"}} />
                  </Link>
                </Row>

                <Row>
                  <h5>Transaction No. {id}</h5>
                </Row>

                <Row>
                  <Form onSubmit={handleSubmit}>
                    <Form.Label>
                      <h6>Confirm the order by entering the receipt number</h6>
                      </Form.Label>
                    <Form.Control type="text" onChange={e => setResi(e.target.value)} className="text-center mt-2" />
                    <Button 
                      type="submit" 
                      className="color-light background-primary outline-light btn-lg mt-4"
                    >
                      Submit
                    </Button>
                  </Form>
                </Row>
              </div>
            )
          } 
        })
        : null
      } 
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  )
}

export default SellerConfirm
