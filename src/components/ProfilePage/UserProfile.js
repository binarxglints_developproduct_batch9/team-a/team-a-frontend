import React, { useState, useEffect } from "react";
import StarRatingComponent from "react-star-rating-component";
import "../assets/css/UserProfile.scss";
import { Card, Image } from "react-bootstrap";
import ModalEditProfile from "../ProfilePage/ModalEditProfile";
import axios from "axios";
function UserProfile() {
  const [users, setUsers] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");

  useEffect(() => {
    axios
      .get(
        `https://sayonara.kuyrek.com/profile/`
      )
      .then((rest) => {
        setUsers(rest.data.data);
        setLoad(true);
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  console.log(users, 'profile')

  return (
    <div>
      <Card key={users.id} className="carduser">
        <Image
          className="imguser"
          variant="top"
          src={`https://sayonara.kuyrek.com/${users.image}`}
          alt="pic"
          roundedCircle
        />
        <div className="usernameuser">
          <h1>{users.username}</h1>
          <StarRatingComponent
            editing={false}
            starCount={5}
            value={users.rating}
            className="starrates"
          />
        </div>
        <ModalEditProfile users={users} />
      </Card>
    </div>
  );
}

export default UserProfile;
