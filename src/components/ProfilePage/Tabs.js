import React, { useState } from "react";
import SellerProduct from "./SellerProduct";
import { Tabs, Tab, Button } from "react-bootstrap";
import Notification from "./Notification";
import ModalCreateProduct from "../modaluser/ModalCreateProduct";
import "../assets/css/Tabs.scss";
import Transaction from "./Transaction";

function TabsDashboard() {
  const [key, setKey] = useState("product");
  const [modalCreateProduct, setModalCreateProduct] = useState(false);
  const toggleModalCreateProduct = () =>
    setModalCreateProduct(!modalCreateProduct);
  const toggleSwitch = () => {
    setModalCreateProduct(!modalCreateProduct);
  };
  return (
    <div className="sellprod pl-4" >
      <div className="row">
        <div className="col-xs-8">
          <button className="createbtn" onClick={toggleModalCreateProduct}>
            CREATE PRODUCT
          </button>
          <ModalCreateProduct
            modal={modalCreateProduct}
            toggleModalCreateProduct={toggleModalCreateProduct}
            toggleSwitch={toggleSwitch}
          />
          <Tabs
            variant="pills"
            className="tabssett"
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
          >
            <Tab tabClassName="tabbtn" eventKey="product" title="Product">
              <SellerProduct />
            </Tab>
            <Tab tabClassName="tabbtn" eventKey="notif" title="Notification">
              <Notification />
            </Tab>
            <Tab
              tabClassName="tabbtn"
              eventKey="transaction"
              title="Transaction History"
            >
              <Transaction />
            </Tab>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
export default TabsDashboard;
