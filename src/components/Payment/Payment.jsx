import React, { useState, useEffect } from "react";
import { FaArrowLeft, FaExclamationCircle } from "react-icons/fa";
import { Link, useParams } from "react-router-dom";
import "../assets/css/payment.scss";
import bni from "../assets/images/bni.png";
import visa from "../assets/images/visa.png";
import master from "../assets/images/mastercard.png";
import mandiri from "../assets/images/Mandiri_logo.png";
import bca from "../assets/images/bca.png";
import permata from "../assets/images/Logo_Bank_Permata.png";
import axios from "axios";

function Payment() {
  const { transactionId, total } = useParams();
  const [image, setImage] = useState({});
  const [submit, setSubmit] = useState(false);

  const handleFileUpload = (event) => {
    const file = event.currentTarget.files[0];
    setImage({
      file: event.currentTarget.files[0],
    });
    setSubmit(true);
  };

  const handleUpload = (e) => {
    e.preventDefault();

    const fd = new FormData();

    fd.append("payment", image.file, image.file.payment);

    for (var pair of fd.entries()) {
      console.log(pair[0] + ", " + pair[1]);
    }

    const urlPay = `https://sayonara.kuyrek.com/transaction/update/payment/${transactionId}`;
    axios
      .put(urlPay, fd)
      .then((res) => {
        console.log(res, "test res dari API");
        window.location.href = "/";
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };

  const calculateTimeLeft = () => {
    let year = new Date().getFullYear();
    const difference = +new Date(`${year}-10-1`) - +new Date();
    let timeLeft = {};

    if (difference > 0) {
      timeLeft = {
        // days: Math.floor(difference / (1000 * 60 * 60 * 24)),
        hours: Math.floor((difference / (1000 * 60 * 60)) % 5),
        minutes: Math.floor((difference / 1000 / 60) % 60),
        seconds: Math.floor((difference / 1000) % 60),
      };
    }

    return timeLeft;
  };

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft());
  const [year] = useState(new Date().getFullYear());

  useEffect(() => {
    setTimeout(() => {
      setTimeLeft(calculateTimeLeft());
    }, 1000);
    return () => {
      clearInterval(setTimeout);
    };
  });

  const timerComponents = [];

  Object.keys(timeLeft).forEach((interval) => {
    if (!timeLeft[interval]) {
      return;
    }

    timerComponents.push(
      <span>
        {timeLeft[interval]} {interval}{" "}
      </span>
    );
  });

  return (
    <body>
      <div className="wrapper-ss">
        <div className="card-payment">
          <div className="header-payment">
            <Link to="/profilepage">
              <button className="btn-back-payment">
                <FaArrowLeft />
              </button>
            </Link>

            <p>
              Transaction No. {transactionId} <br />
              Payment Info
            </p>
            <hr />
          </div>
          <div className="box-payment-info">
            <p>Total Payment:</p>
            <span>Rp {priceForm(total)} </span>
            <div className="p-payment-info">
              <p>
                <FaExclamationCircle /> Please pay according to the amount above{" "}
              </p>
            </div>
            <div>
              {timerComponents.length ? (
                timerComponents
              ) : (
                <span>Your transactions has been cancelled</span>
              )}
            </div>
            <p>
              Payment will be checked within 24 hours after the proof of payment
              is uploaded.
            </p>
            <div className="payment-guide">
              <ol>
                <li>
                  Use ATM/iBanking/mBanking/cash deposit for transfer to
                  Sayonara bank accounts <mark className="background-primary">130945863</mark>
                  <div className="bank-logo d-flex flex-row">
                    <img src={bni} alt="bank" className="logo-bni" />
                    <img src={visa} alt="bank" className="logo-bank" />
                    <img src={master} alt="bank" className="logo-master" />
                    <img src={mandiri} alt="bank" className="logo-mandiri" />
                    <img src={bca} alt="bank" className="logo-bca" />
                    <img src={permata} alt="bank" className="logo-prmt" />
                  </div>
                </li>
                <li>
                  The order will be automatically cancelled within 2 hours if
                  the proof of payment did not submitted
                </li>
                <li>
                  Please upload proof of transfer before the expiration date
                </li>
                <li>
                  For the sake of transaction security, please do not share
                  proof or payment confirmation with anyone, other than
                  uploading it through the Sayonara website
                </li>
              </ol>
            </div>
            <div>
              <form onSubmit={handleUpload}>
                <input
                  type="file"
                  id="actual-btn"
                  accept="image/*"
                  hidden
                  onChange={(e) => handleFileUpload(e)}
                />
                <label
                  className="btn-upload-payment text-center "
                  for="actual-btn"
                  type="submit"
                >
                  {submit == false ? (
                    <label>Upload proof of transfer</label>
                  ) : (
                    <button
                      type="submit"
                      className="btn-payment background-primary"
                    >
                      Succesfully submitted! Click here to proceed
                    </button>
                  )}
                </label>
              </form>
            </div>
          </div>
        </div>
      </div>
    </body>
  );
}
export default Payment;
