import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import { FaEdit } from 'react-icons/fa'

const UserDetails = (props) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const userName = props.username

  const [user, setUser] = useState({
    fullName: '',
    username: '',
    image: '',
    role: '',
    email: '',
    isVerified: ''
  })

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com`)
    .then(
      res => {
        const result = res.data.data
        result.map(item => {
          if(item.username === userName) {
            setUser({
              fullName: item.fullName,
              username: item.username,
              image: item.image,
              role: item.role,
              email: item.email,
              isVerified: item.isVerified
            })
          }
        })
      }
    )
    .catch(err => console.log(err))
  }, [])
  
  const [file, setFile] = useState({
    selectedFile: null,
  });

  const handleFile = (e) => {
    setFile({
      selectedFile: e.target.files[0],
    });
  };

  let fullName = user.fullName
  let username = user.username
  let role = user.role
  let email = user.email
  let verification = user.isVerified

  const handleSubmit = e => {
    e.preventDefault()

    const fd = new FormData();

    fd.append("fullName", fullName)
    fd.append("username", username)
    fd.append("role", role)
    fd.append("email", email)
    fd.append("isVerified", verification)
    // fd.append("image", file.selectedFile, file.selectedFile.name)
    file.selectedFile === null ? alert('Please upload image') : fd.append("image", file.selectedFile, file.selectedFile.name)

    axios.put(`https://sayonara.kuyrek.com/update/${userName}`, fd)
    .then(
      res => {
        alert(res.data.status)
        window.location.reload()
      }
    )
    .catch(
      err => {
        console.log(err)
      }
    )
  }

  return (
    <>
      <Button 
      className="color-light background-primary outline-primary mr-1" 
      onClick={handleShow}
      >
        <FaEdit className="mb-1" />
      </Button>
      
      <Modal show={show} onHide={handleClose}>
        <Form onSubmit={handleSubmit}>
          <Modal.Header closeButton>
            <Modal.Title>Update User</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group>
              <Form.Label>FullName</Form.Label>
              <Form.Control 
                type="text" 
                defaultValue={user.fullName}
                onChange={e => fullName = e.target.value}
              />
            </Form.Group>
            
            <Form.Group>
              <Form.Label>Username</Form.Label>
              <Form.Control 
                type="text" 
                defaultValue={user.username} 
                onChange={e => username = e.target.value}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Image</Form.Label>
              <Form.Control 
                accept="image/*"
                type="file" 
                onChange={e => handleFile(e)}
              />
            </Form.Group>
            
            <Form.Group>
              <Form.Label>Role</Form.Label>
              <Form.Control 
                as="select" 
                onChange={e => role = e.target.value}
                defaultValue={user.role}
              >
                <option>Please select...</option>
                <option value="user">User</option>
                <option value="admin">Admin</option>
              </Form.Control>
            </Form.Group>

            <Form.Group>
              <Form.Label>Email</Form.Label>
              <Form.Control 
                type="email" 
                defaultValue={user.email} 
                onChange={e => email = e.target.value}
              />
            </Form.Group>
            
            <Form.Group>
              <Form.Label>Verified</Form.Label>
              <Form.Control 
                as="select"
                defaultValue={user.isVerified}
                onChange={e => verification = e.target.value}
              >
                <option>Please select...</option>
                <option value="true">True</option>
                <option value="false">False</option>
              </Form.Control>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button 
              className="color-primary background-light outline-primary" 
              type="submit"
            >
              Submit
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  )
}

export default UserDetails
