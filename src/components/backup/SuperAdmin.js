import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { Col, Container, Image, Row } from 'react-bootstrap'
import UserDelete from './UserDelete'
import UserDetails from './UserDetails'

const SuperAdmin = () => {
  const [data, setData] = useState([])

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com`)
      .then(
        res => {
          setData(res.data.data)
        }
      )
      .catch(err => console.log(err))
  }, [])

  return (
    <Container className="d-flex flex-wrap text-center my-5 p-5 bg-white">
      <Row className="justify-content-center">
        <Col md="2">
          <h6>ID</h6>
        </Col>

        <Col md="2">
          <h6>Image</h6>
        </Col>

        <Col md="2">
          <h6>Username</h6>
        </Col>

        <Col md="2">
          <h6>Created At</h6>
        </Col>

        <Col md="2">
          <h6>Action</h6>
        </Col>
      </Row>
      {
        data
        ? data.map(item => {
          const { username, createdAt, image, id } = item
          return (
            <Row key={id} className="justify-content-center my-2">
              <Col md="2" className="pt-3">
                <p>{id}</p>
              </Col>

              <Col md="2" className="">
                <Image src={`https://sayonara.kuyrek.com${image}`} style={{width: "4rem", height: "4rem"}} />
              </Col>

              <Col md="2" className="pt-3">
                <p>{username}</p>
              </Col>

              <Col md="2" className="pt-3">
                <p>{createdAt.split('T')[0]}</p>
              </Col>

              <Col md="2" className="pt-3">
                <UserDetails username={username} />
                <UserDelete username={username} />
              </Col>
            </Row>
          )
        })
        : null
      }
    </Container>
  )
}

export default SuperAdmin
