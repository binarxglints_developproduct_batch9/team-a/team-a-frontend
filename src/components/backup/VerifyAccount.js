import axios from 'axios'
import React, { useEffect } from 'react'
import { Container } from 'react-bootstrap'
import { Link, useParams } from 'react-router-dom'

const VerifyAccount = () => {
  const { token } = useParams()
  
  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/verification/${token}`)
    .then(
      res => {
        console.log(res)
      }
    )
    .catch(
      err => {
        console.log(err)
      }
    )
  }, [])
  
  return (
    <Container className="bg-white d-flex justify-content-center mt-5 p-5">
      <p>
        Your account is verified. Please <Link className="color-primary text-decoration-none" to="/signin">sign in</Link> to Continue.
      </p>
      
    </Container>
  )
}

export default VerifyAccount
