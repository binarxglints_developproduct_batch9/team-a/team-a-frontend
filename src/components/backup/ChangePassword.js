import axios from 'axios'
import React, { useState } from 'react'
import { Button, Form, Modal } from 'react-bootstrap'
import Header from '../Homepage/Header'

const ChangePassword = () => {
  let currentPassword, newPassword, confirmNewPassword
  
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === false) window.location.reload()
  }

  const handleSubmit = e => {
    e.preventDefault()

    const data = {
      oldPassword: currentPassword,
      newPassword: newPassword,
      newPasswordConfirmation: confirmNewPassword
    }

    axios.put(`https://sayonara.kuyrek.com/change_password`, data)
      .then(
        res => {
          setError(false)
          setTitle('Success')
          setMessage(res.data.status)
          setModal(true)
        }
      )
      .catch(
        err => {
          setError(true)
          setTitle('Error')
          setMessage('Please try again.')
          setModal(true)
        }
      )
  }

  return (
    <>
      <Header />
      <div className="header-container bg-white d-flex flex-wrap justify-content-center p-5">
        <Form className="w-25" onSubmit={handleSubmit}>
          <Form.Text className="mb-3"><h3 className="color-primary">Change Password</h3></Form.Text>

          <Form.Group>
            <Form.Label>Current Password</Form.Label>
            <Form.Control type="password" placeholder="Current Password" onChange={e => currentPassword = e.target.value} />
          </Form.Group>

          <Form.Group>
            <Form.Label>New Password</Form.Label>
            <Form.Control type="password" placeholder="New Password" onChange={e => newPassword = e.target.value} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Confirm New Password</Form.Label>
            <Form.Control type="password" placeholder="Confirm New Password" onChange={e => confirmNewPassword = e.target.value} />
          </Form.Group>

          <Button type="submit" className="color-light background-primary outline-primary">Change Password</Button>
        </Form>
      </div>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default ChangePassword
