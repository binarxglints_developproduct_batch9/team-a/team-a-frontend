import axios from 'axios'
import React, { useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from "../assets/images/Sayonara.png"

const SignIn = () => {
  let username, password
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === false) window.location.href = '/'
  }

  const handleSubmit = e => {
    e.preventDefault()

    const data = {
      emailOrUsername: username,
      password: password
    }

    axios.post('https://sayonara.kuyrek.com/login', data)
      .then(
        res => {
          setError(false)
          setTitle('Success')
          setMessage('Welcome to Sayonara. Have a nice day!')
          setModal(true)
          localStorage.setItem('token', res.data.token)
          localStorage.setItem("username", username)
          if(username === 'falhar') {
            window.location.href = '/superadmin'
          }
        }
      )
      .catch(
        err => {
          if(err.message) {
            setError(true)
            setTitle('Error')
            setMessage('Username or password is incorrect.')
            setModal(true)
          }
        }
      )
  }

  return (
    <Row className="justify-content-center mt-5">
      <Link className="w-40 my-5 mr-5" to="/">
        <img src={logo} alt="logo" className="img-fluid pt-5" style={{width:"600px", height:"auto"}} />
      </Link>
      <Col md="4" className="bg-white p-5 my-auto rounded">
        <Form onSubmit={handleSubmit}>
          <h3 className="color-primary text-center">Sign In</h3>
          <Form.Group>
            <Form.Label>Username or Email</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Username or Email" 
              onChange={e => username = e.target.value}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Password" 
              onChange={e => password = e.target.value}
            />
          </Form.Group>

          <Form.Group>
            <Form.Text>
              <Link className="text-decoration-none" to="/forgot">
              <span className="color-primary">Forgot Password?</span>
              </Link>
            </Form.Text>
            <Form.Text>
              Have no account yet? &nbsp;
              <Link className="text-decoration-none" to="/signup">
                <span className="color-primary">Sign Up</span>
              </Link>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Button type="submit" className="background-primary outline-primary">Sign In</Button>
          </Form.Group>
        </Form>
      </Col>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  )
}

export default SignIn
