import axios from 'axios'
import React, { useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from "../assets/images/Sayonara.png"

const SignUp = () => {
  let username, email, password, cPassword
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === false) window.location.href = '/signin'
  }

  const handleSubmit = e => {
    e.preventDefault()
    
    const data = {
      username: username,
      email: email,
      password: password,
      passwordConfirmation: cPassword
    }

    axios.post('https://sayonara.kuyrek.com/signup', data)
      .then(
        res => {
          setTitle('')
          setMessage(res.data.message)
          setError(false)
          setModal(true)
        }
      )
      .catch(
        err => {
          setError(true)
          setTitle('Error')
          console.log(err.message)
          if(err.message)
            setMessage('Username or email is registered. Please use another one.')
          setModal(true)
        }
      )
  }

  return (
    <Row className="d-flex flex-wrap justify-content-center mt-5">
      <Link className="w-40 my-5 mr-5" to="/">
        <img src={logo} alt="logo" className="img-fluid pt-5" style={{width:"600px", height:"auto"}} />
      </Link>
      <Col md="4" className="bg-white p-5 my-auto rounded">
        <Form onSubmit={handleSubmit} action="/signin">
          <h3 className="color-primary text-center">Sign Up</h3>
          <Form.Group>
            <Form.Label>Username</Form.Label>
            <Form.Control 
              type="text" 
              placeholder="Username" 
              onChange={e => username = e.target.value}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control 
              type="email" 
              placeholder="Email" 
              onChange={e => email = e.target.value}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Password" 
              onChange={e => password = e.target.value}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Confirm Password" 
              onChange={e => cPassword = e.target.value}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Text>
              Already have an account? &nbsp;
              <Link className="text-decoration-none" to="/signin">
                <span className="color-primary">Sign In</span>
              </Link>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Button type="submit" className="background-primary outline-primary">Sign Up</Button>
          </Form.Group>
        </Form>
      </Col>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  )
}

export default SignUp
