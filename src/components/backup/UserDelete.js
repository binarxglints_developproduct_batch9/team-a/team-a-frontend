import axios from 'axios';
import React, { useState } from 'react'
import { Button, Modal } from 'react-bootstrap';
import { FaTrash } from 'react-icons/fa'

const UserDelete = (props) => {
  const [show, setShow] = useState(false)

  const handleClose = () => setShow(false)
  const handleShow = () => setShow(true)

  const username = props.username

  const handleDelete = () => {
    axios.delete(`https://sayonara.kuyrek.com/hard_delete/${username}`)
    .then(
      res => {
        alert(res.data.message)
        window.location.href = '/superadmin'
      }
    )
    .catch(
      err => {
        console.log(err)
      }
    )
  }

  return (
    <>
      <Button 
        variant="danger"
        onClick={handleShow}
      >
        <FaTrash className="mb-1" />
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Delete User "{username}"</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure to delete this user?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="danger" onClick={handleDelete}>
            Yes, I'm sure
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
}

export default UserDelete
