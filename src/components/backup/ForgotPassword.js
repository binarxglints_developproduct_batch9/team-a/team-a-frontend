import axios from 'axios'
import React, { useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from "../assets/images/Sayonara.png"

const ForgotPassword = () => {
  let email
  const [modal, setModal] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => setModal(false)
  
  const handleSubmit = e => {
    e.preventDefault()

    console.log(email)

    axios.post(`https://sayonara.kuyrek.com/forgot_password`, { email: email })
      .then(
        res => {
          setTitle('')
          setMessage(`${res.data.message}.`)
          setModal(true)
        }
      )
      .catch(
        err => {
          if(err) {
            setTitle('Error')
            setMessage('Email is not registered.')
            setModal(true)
          }
        }
      )
  }

  return (
    <Row className="justify-content-center mt-5">
      <Link className="w-40 my-5 mr-5" to="/">
        <img src={logo} alt="logo" className="img-fluid pt-5" style={{width:"600px", height:"auto"}} />
      </Link>
      <Col md="4" className="bg-white p-5 my-auto rounded">
        <Form onSubmit={handleSubmit}>
          <h3 className="color-primary text-center">Forgot Password</h3>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control 
              type="email" 
              placeholder="Email" 
              onChange={e => email = e.target.value}
              required
            />
          </Form.Group>

          <Form.Group>
            <Form.Text>
              Already have an account? &nbsp;
              <Link className="text-decoration-none" to="/signin">
                <span className="color-primary">Sign In</span>
              </Link>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Button type="submit" className="background-primary outline-primary">Send Request</Button>
          </Form.Group>
        </Form>
      </Col>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  )
}

export default ForgotPassword
