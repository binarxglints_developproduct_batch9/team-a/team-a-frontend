import axios from 'axios'
import React, { useState } from 'react'
import { Button, Col, Form, Modal, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import logo from "../assets/images/Sayonara.png"
import { useParams } from 'react-router-dom'

const ResetPassword = () => {
  const token = useParams().token

  let password, cPassword
  const [modal, setModal] = useState(false)
  const [error, setError] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const closeModal = () => {
    setModal(false)
    if(error === false) window.location.href = '/signin'
  }

  const handleSubmit = e => {
    e.preventDefault()

    const data = {
      newPassword: password,
      newPasswordConfirmation: cPassword
    }
    
    axios.put(`https://sayonara.kuyrek.com/reset_password/${token}`, data)
      .then(
        res => {
          setError(false)
          setTitle(res.data.status)
          setMessage(`Sign in with the new password to continue.`)
          setModal(true)
        }
      )
      .catch(
        err => {
          if(err) {
            setError(true)
            setTitle('Error')
            setMessage('Passwords do not match.')
            setModal(true)
          }
        }
      )
  }

  return (
    <Row className="justify-content-center mt-5">
      <Link className="w-40 my-5 mr-5" to="/">
        <img src={logo} alt="logo" className="img-fluid pt-5" style={{width:"600px", height:"auto"}} />
      </Link>
      <Col md="4" className="bg-white p-5 my-auto rounded">
        <Form onSubmit={handleSubmit}>
          <h3 className="color-primary text-center">Reset Password</h3>
          <Form.Group>
            <Form.Label>Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Password" 
              onChange={e => password = e.target.value}
            />
          </Form.Group>
          
          <Form.Group>
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control 
              type="password" 
              placeholder="Confirm Password" 
              onChange={e => cPassword = e.target.value}
            />
          </Form.Group>

          <Form.Group>
            <Form.Text>
              Already have an account? &nbsp;
              <Link className="text-decoration-none" to="/signin">
                <span className="color-primary">Sign In</span>
              </Link>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            <Button type="submit" className="background-primary outline-primary">Reset</Button>
          </Form.Group>
        </Form>
      </Col>
      <Modal 
        show={modal} 
        onHide={closeModal}
        backdrop="static"
        keyboard={false}
      >
        <Modal.Header closeButton>
          <Modal.Title>{title}</Modal.Title>
        </Modal.Header>
        <Modal.Body>{message}</Modal.Body>
        <Modal.Footer>
          <Button 
            className="color-primary background-light outline-primary" 
            onClick={closeModal}
          >
            OK
          </Button>
        </Modal.Footer>
      </Modal>
    </Row>
  )
}

export default ResetPassword
