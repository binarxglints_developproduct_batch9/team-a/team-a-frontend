import { Button, Modal } from "react-bootstrap";
import React, { useState } from "react";
import styles from "./edit.module.css";
import axios from "axios";

function ModalEditProduct(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  console.log(props.item._id, "red");
  let name = props.item.name;
  let desc = props.item.desc;
  let category = props.item.category;
  let price = props.item.price.$numberDecimal;
  let quantity = props.item.stock.$numberDecimal;
  let images = props.item.image;
  const [file, setFile] = useState({
    selectedFile: null,
  });

  const handleFile = (e) => {
    setFile({
      selectedFile: e.target.files[0],
    });
  };

  const handleUpload = (e) => {
    e.preventDefault();

    const fd = new FormData();

    fd.append("name", name);
    fd.append("price", price);
    fd.append("desc", desc);
    fd.append("stock", quantity);
    fd.append("category", category);
    fd.append("image", file.selectedFile, file.selectedFile.name);

    axios
      .put(`https://sayonara.kuyrek.com/home/update/${props.item._id}`, fd)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => console.log(err));
  };
  const [picture, setPicture] = useState(null);
  const [img, setImg] = useState(null);

  const onChangePicture = (e) => {
    if (e.target.files[0]) {
      setPicture(e.target.files[0]);
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setImg(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };

  return (
    <div>
      <Button className={styles.editbtn} onClick={handleShow}>
        EDIT
      </Button>

      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Edit Product</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <form onSubmit={handleUpload} id="editpform">
            <img className={styles.preview} src={img} />
            <input
              accept="image/png, image/jpeg, image/jpg"
              type="file"
              id="actual-btn"
              onChange={(e) => {
                onChangePicture(e);
                handleFile(e);
              }}
              hidden
            />
            <label className={styles.label} for="actual-btn">
              UPLOAD IMAGE
            </label>
            <p className={styles.tagged}>Product Name</p>
            <input
              type="text"
              onChange={(e) => (name = e.target.value)}
              className={styles.proinput}
              // placeholder={props.item.name}
              defaultValue={name}
            ></input>
            <p className={styles.tagged}>Category</p>
            <select
              className={styles.proselect}
              type="text"
              id="category"
              name="category"
              onChange={(e) => (category = e.target.value)}
              defaultValue={category}
              form="editpform"
            >
              {" "}
              <option value="">{""}</option>
              <option value="fruit">fruit</option>
              <option value="vegetable">vegetable</option>
            </select>
            <p className={styles.tagged}>Price</p>
            <input
              type="number"
              onChange={(e) => (price = e.target.value)}
              className={styles.proinput}
              defaultValue={price}
            ></input>{" "}
            <p className={styles.tagged}>Stock</p>
            <input
              type="number"
              onChange={(e) => (quantity = e.target.value)}
              className={styles.proinput}
              defaultValue={quantity}
            ></input>{" "}
            <p className={styles.tagged}>Description</p>
            <textarea
              type="text"
              onChange={(e) => (desc = e.target.value)}
              className={styles.proinputt}
              defaultValue={desc}
            ></textarea>
            <Button type="submit" className={styles.saveedt}>
              SAVE CHANGES
            </Button>
          </form>
        </Modal.Body>
      </Modal>
    </div>
  );
}

export default ModalEditProduct;
