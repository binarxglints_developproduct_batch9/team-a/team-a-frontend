import React, { useState } from "react";
import { Modal, ModalBody, ModalHeader, ModalFooter } from "reactstrap";
import styles from "./product.module.css";
import axios from "axios";

function ModalCreateProduct(props) {
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");

  let name, desc, category, price, quantity;

  const [file, setFile] = useState({
    selectedFile: null,
  });

  const handleFile = (e) => {
    setFile({
      selectedFile: e.target.files[0],
    });
  };

  const handleUpload = (e) => {
    e.preventDefault();

    const fd = new FormData();

    fd.append("name", name);
    fd.append("price", price);
    fd.append("desc", desc);
    fd.append("stock", quantity);
    fd.append("category", category);
    fd.append("image", file.selectedFile, file.selectedFile.name);

    axios
      .post("https://sayonara.kuyrek.com/home/create/", fd)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const [picture, setPicture] = useState(null);
  const [img, setImg] = useState(null);

  const onChangePicture = (e) => {
    if (e.target.files[0]) {
      setPicture(e.target.files[0]);
      const reader = new FileReader();
      reader.addEventListener("load", () => {
        setImg(reader.result);
      });
      reader.readAsDataURL(e.target.files[0]);
    }
  };
  return (
    <div className="hiwk">
      <Modal isOpen={props.modal} toggle={props.toggleModalCreateProduct}>
        <ModalHeader toggle={props.toggleModalCreateProduct}>
          Create Product
        </ModalHeader>
        <ModalBody>
          {/* https://codesandbox.io/s/red-cherry-xjbul?file=/src/App.js */}
          <form onSubmit={handleUpload} id="creform">
            <img className={styles.preview} src={img} />
            <input
              accept="image/*"
              type="file"
              onChange={(e) => {
                onChangePicture(e);
                handleFile(e);
              }}
              id="actual-btn"
              hidden
            />
            <label className={styles.label} for="actual-btn">
              UPLOAD IMAGE
            </label>
            <p className={styles.tagged}>Product Name</p>
            <input
              className={styles.proinput}
              type="text"
              id="productName"
              name="productName"
              onChange={(e) => (name = e.target.value)}
            ></input>
            <p className={styles.tagged}>Category</p>
            <select
              className={styles.proselect}
              type="text"
              id="category"
              name="category"
              onChange={(e) => (category = e.target.value)}
              form="creform"
            >
              {" "}
              <option value="">{""}</option>
              <option value="fruit">Fruit</option>
              <option value="vegetable">Vegetable</option>
            </select>
            <p className={styles.tagged}>Price</p>
            <input
              className={styles.proinput}
              type="text"
              id="price"
              name="price"
              onChange={(e) => (price = e.target.value)}
            ></input>
            <p className={styles.tagged}>Stock</p>
            <input
              className={styles.proinput}
              type="text"
              id="stock"
              name="stock"
              onChange={(e) => (quantity = e.target.value)}
            ></input>
            <p className={styles.tagged}>Description</p>
            <textarea
              className={styles.proinputt}
              type="text"
              id="description"
              name="description"
              onChange={(e) => (desc = e.target.value)}
            ></textarea>
            <button type="submit" className={styles.btncre}>
              CREATE
            </button>
          </form>
        </ModalBody>
      </Modal>
    </div>
  );
}
export default ModalCreateProduct;
