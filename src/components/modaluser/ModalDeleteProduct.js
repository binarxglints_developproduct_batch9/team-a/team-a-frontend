import { Button, Modal } from "react-bootstrap";
import React, { useState } from "react";
import style from "./delete.module.css";
import axios from "axios";

function ModalDeleteProduct(props) {
  const [show, setShow] = useState(false);

  const handleDelete = (e) => {
    e.preventDefault();
    axios
      .delete(`https://sayonara.kuyrek.com/home/delete/${props.item._id}`)
      .then((res) => {
        window.location.reload();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div className="delbtn">
      <Button className={style.deletebtn} onClick={() => setShow(true)}>
        DELETE
      </Button>
      <div>
        <Modal
          show={show}
          onHide={() => setShow(false)}
          {...props}
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton></Modal.Header>
          <div className="text-center">
            <h3>
              Delete <b>{`${props.item.name}`}</b>
            </h3>
            <h6 className={style.caution}>This action cannot be undone</h6>
          </div>
          <Modal.Footer>
            <Button className={style.delsu} onClick={handleDelete}>
              DELETE
            </Button>
            <Button className={style.candel} onClick={props.onHide}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </div>
  );
}

export default ModalDeleteProduct;
