import React, { useEffect, useState } from "react";
import axios from "axios";
import SellerProduct from "./SellerProduct";
import { Tabs, Tab } from "react-bootstrap";
import "../assets/css/TabsSellProd.scss";
import { useParams } from "react-router-dom";
import SellerProductVeg from "./SellerProductVeg";
import SellerProductFruit from "./SellerProductFruit";

function TabsDashboard() {
  const [key, setKey] = useState("all");
  const [product, setProduct] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");
  const username = useParams().username
  useEffect(() => {
    axios
      .get(`https://sayonara.kuyrek.com/home/seller/${username}`)
      .then((res) => {
        setProduct(res.data.data);
        setLoad(true);
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);
  return (
    <div className="sellprodc pl-4">
      <div className="row">
        <div className="col-xs-8 pt-4">
          <Tabs
            variant="pills"
            tabClassName="tabssett"
            id="controlled-tab-example"
            activeKey={key}
            onSelect={(k) => setKey(k)}
            defaultActiveKey="all"
          >
            <Tab tabClassName="tabbtn" eventKey="all" title="All">
              <SellerProduct />
            </Tab>
            <Tab
              tabClassName="tabbtn"
              eventKey="fruit"
              title="Fruits"
            >
              <SellerProductFruit />
            </Tab>
            <Tab tabClassName="tabbtn" eventKey="vegetable" title="Vegetables">
              <SellerProductVeg />
            </Tab>
          </Tabs>
        </div>
      </div>
    </div>
  );
}
export default TabsDashboard;
