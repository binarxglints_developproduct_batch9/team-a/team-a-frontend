import React, { useEffect, useState } from "react";
import axios from "axios";
import "../assets/css/SellerProduct.scss";
import { Button, Card } from "react-bootstrap";
import { Link, useParams } from "react-router-dom";
function SellerProduct() {
  const [product, setProduct] = useState([]);
  const [load, setLoad] = useState(false);
  const [error, setError] = useState(" ");

  const username = useParams().username
 
  useEffect(() => {
    axios
      .get(`https://sayonara.kuyrek.com/home/seller/${username}`)
      .then((res) => {
        console.log(res)
        setProduct(res.data.data);
        setLoad(true);
        {console.log(res.data.data)}
      })
      .catch((err) => {
        setError(err.message);
        setLoad(true);
      });
  }, []);

  const priceForm = (num) => {
    let str = num.toString(),
      split = str.split(","),
      sisa = split[0].length % 3,
      rupiah = split[0].substr(0, sisa),
      ribuan = split[0].substr(sisa).match(/\d{3}/gi);
    // jika angka telah mencapai ribuan(1000), maka akan ditambahkan titik (1.000)
    if (ribuan) {
      let separator = sisa ? "." : "";
      rupiah += separator + ribuan.join(".");
    }
    rupiah = split[1] !== undefined ? rupiah + "," + split[1] : rupiah;
    return rupiah;
  };

  const addCart = (e, id) => {
    axios.post(`https://sayonara.kuyrek.com/cart/create`, { productId: id })
      .then(
        res => {
          alert(res.data.status)
        }
      )
      .catch(
        err => {
          if(err.message === 'Request failed with status code 401') {
            alert('Please Sign In first!')
            window.location.href = '/signin'
          }
          else if(err.message === 'Request failed with status code 422') {
            alert(`You can't buy your own product!`)
          }
        }
      )
  }

  return (
    <div>
      <div class="scss-sellerproduct">
        {product.length
          ? product.map(item => (
              <div class="cardsize" key={item._id}>
                <Card style={{ width: "20rem" }}>
                  <Link className="linkto text-decoration-none" to={`/productDetail/${item._id}`}>
                    <Card.Img
                      class="img-size"
                      src={`https://sayonara.kuyrek.com${item.image}`}
                      alt="product"
                    />
                  </Link>
                  <Card.Body className="prodbodd d-flex flex-column">
                      <h1 class="mt-auto item-name">{item.name}</h1>
                      <p class="price">
                        Rp.{" "}
                        {priceForm(item.price.$numberDecimal)}
                        ,-
                      </p> 
                      <Button className="mx-auto color-primary background-light outline-primary" onClick={e => addCart(e, item.id)}>Add to cart</Button>
                    </Card.Body>
                </Card>
              </div>
            ))
          : null}
        {!product.length && "There isn't any product in this category"}
      </div>
    </div>
  );
}
export default SellerProduct;
