import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Image } from "react-bootstrap";
import { useParams } from "react-router-dom";
import StarRatingComponent from "react-star-rating-component";
import "../assets/css/UserProfile.scss"
function UserProfile() {
  const seller = useParams().username

  const [data, setData] = useState({})

  useEffect(() => {
    axios.get(`https://sayonara.kuyrek.com/profile/${seller}`)
      .then(
        res => {
          const result = res.data.data
          setData({ result })
        }
      )
  }, [])

  const { result } = data
  console.log(result)
  
  const image = result === undefined ? null : result.image
  const username = result === undefined ? null : result.username
  const rating = result === undefined ? null : result.rating

  return (
    <div>
      <Card className="carduser">
        <Image
          className="imguser"
          variant="top"
          src={`https://sayonara.kuyrek.com/${image}`}
          alt="pic"
          roundedCircle
        />
        <div className="usernameuser">
          <h1>{username}</h1>
          <StarRatingComponent
            editing={false}
            starCount={5}
            value={rating}
            className="starrates"
          />
        </div>
      </Card>
    </div>
  );
}

export default UserProfile;
