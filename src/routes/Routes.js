import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import PageProduct from "../pages/PageProduct.jsx"
import Homepage from "../pages/Homepage";
import ProfilePage from "../pages/ProfilePage";
import SearchResultPage from "../pages/SearchResultPage";
// import SignIn from "../components/Homepage/sign-in";
// import SignUp from "../components/Homepage/sign-up";
import PagePayment from "../pages/PagePayment.jsx"
import PageCart from "../pages/PageCart";
import Test from "../components/Test";
import Page404 from "../pages/Page404.jsx"
import Login from '../components/backup/SignIn'
import Register from '../components/backup/SignUp'
import SellerPage from "../pages/SellerPage";
import SellerConfirmPage from "../pages/SellerConfirmPage";
import ForgotPassword from "../components/backup/ForgotPassword";
import ResetPassword from "../components/backup/ResetPassword";
import ChangePassword from "../components/backup/ChangePassword.js";
import SuperAdmin from "../components/backup/SuperAdmin.js";
import VerifyAccount from "../components/backup/VerifyAccount.js";

const Routes = () => {
    return (
      <>
        <Router>
            <Switch>
                {/* <Route path="/sign-up" exact>
                    <SignUp />
                </Route>
                <Route path="/sign-in" exact>
                    <SignIn />
                </Route> */}
                <Route path="/" exact component={Homepage} />
                <Route path="/signup" exact component={Register} />
                <Route path="/signin" exact component={Login} />
                <Route path="/forgot" exact component={ForgotPassword} />
                <Route path="/reset_password/:token" exact component={ResetPassword} />
                <Route path="/change_password" exact component={ChangePassword} />
                <Route path="/verification/:token" exact component={VerifyAccount} />
                <Route path="/superadmin" exact component={SuperAdmin} />
                <Route path="/search" exact component={SearchResultPage} />
                <Route path="/profilepage" exact component={ProfilePage} />
                <Route path="/productDetail/:productId" exact component={PageProduct} />
                <Route path="/payment/:transactionId" exact component={PagePayment} />
                <Route path="/payment/:transactionId/:total" exact component={PagePayment} />
                <Route path="/cart" exact component={PageCart} />
                <Route path="/seller/:username" exact component={SellerPage} />
                <Route path="/sellerConfirm/:transactionId" exact component={SellerConfirmPage} />
                <Route path="/test" exact component={Test} />
                <Route path="*" exact component={Page404} />
            </Switch>
        </Router>
      </>
    )
}
export default Routes;