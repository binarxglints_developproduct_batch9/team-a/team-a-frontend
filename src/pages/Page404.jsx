import React from 'react'
import logo from "../components/assets/images/Sayonara.png"
import cat from "../components/assets/images/cute cat 2.png"
import "../components/assets/css/page404.scss"
import { Link } from 'react-router-dom'


function Page404() {
    return (
        <body className="row">
            <Link to="/">
            <img src={logo} alt="logo" className="img-logo-404"/>
            </Link>
            <h1 className="col-md-12 text-center color-accent display-1">404 Errors</h1>
            <img src={cat} alt="cat" className="cat-image"/>
            <h2 className="col-md-12 text-center color-accent">Sorry, we couldn't find the page</h2>
            
        </body>
    )
}

export default Page404
