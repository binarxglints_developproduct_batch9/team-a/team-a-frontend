import React from 'react'
import Footer from '../components/Homepage/Footer'
import Carousels from "../components/Homepage/Carousel"
import ButtonCategory from "../components/Homepage/ButtonCategory"
import Header from '../components/Homepage/Header'

const Homepage = () => {
  return (
    <>
      <Header />
      <Carousels />
      <ButtonCategory />
      <Footer />
    </>
  )
}

export default Homepage
