import React from 'react'
import Header from '../components/Homepage/Header'
import SellerConfirm from '../components/ProfilePage/SellerConfirm'

const SellerConfirmPage = () => {
  return (
    <>
      <Header />
      <SellerConfirm />
    </>
  )
}

export default SellerConfirmPage
