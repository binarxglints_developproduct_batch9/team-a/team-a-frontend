import React from "react";
import Header from "../components/Homepage/Header";
import TabsDashboard from "../components/ProfilePage/Tabs";
import UserProfile from "../components/ProfilePage/UserProfile";
const ProfilePage = () => {
  return (
    <>
    <Header />
    <div class="d-flex flex-wrap justify-content-between mb-4">
      <UserProfile />
      <TabsDashboard />
    </div>
    </>
  );
};

export default ProfilePage;
