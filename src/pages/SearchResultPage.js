import React from 'react'
import Footer from '../components/Homepage/Footer'
import Header from '../components/Homepage/Header'
import SearchResult from '../components/Homepage/SearchResult'

const SearchResultPage = () => {
  return (
    <>
      <Header />
      <SearchResult />
      <Footer />
    </>
  )
}

export default SearchResultPage
