import React from 'react'
import Header from '../components/Homepage/Header'
import TabsDashboard from '../components/Seller/Tabs'
import UserProfile from '../components/Seller/UserProfile'

const SellerPage = () => {
  return (
    <>
      <Header />
      <div class="d-flex flex-wrap justify-content-between mb-4">
      <UserProfile />
      <TabsDashboard />
    </div>
    </>
  )
}

export default SellerPage
