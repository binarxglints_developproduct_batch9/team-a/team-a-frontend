import React from 'react'
import Cart from '../components/Cart/Cart'
import Header from '../components/Homepage/Header'


const PageCart = () => {
    return (
        <div>
          <Header />
           <Cart />
        </div>
    )
}

export default PageCart
