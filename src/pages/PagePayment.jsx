import Payment from "../components/Payment/Payment"
import React from 'react'
import Header from "../components/Homepage/Header"


const PagePayment = () => {
    return (
        <div>
          <Header />
          <Payment />
        </div>
    )
}

export default PagePayment


